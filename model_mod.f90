module model_mod

    !author: wangzhenhua 
    
    !Variable declarations 
    implicit none
    
    ! include mkl_pardiso.fi: solve sparse matrix 
    include 'mkl_pardiso.fi'
    
    !basic model parameters 
    real, parameter         :: Cp = 1.0e3           ! specific heat: J/Kg/K
    real, parameter         :: d = 10e3             ! characteristic length of radioactive heating: m
    real, parameter         :: La = 420e3           ! latent heat of fusion: J/kg
    real, parameter         :: Rgas = 8.3144598     ! gas constant: J/(mol*K)

    
    ! thermal_expansion and hermal compressibility
    real, parameter         :: thermal_expansion = 3e-5         ! thermal expansion coefficient: k**-1
    real, parameter         :: thermal_compressibility = 1e-11  ! thermal compressibility coefficient: pa**-1
    
    ! model size and step 
    real, parameter         :: xsize = 600e3        ! the length of model horizontally: m
    real, parameter         :: ysize = 110e3        ! the length of model vertically: m
    integer, parameter      :: xnum = 601           ! number of nodes along xc
    integer, parameter      :: ynum = 111           ! number of nodes along yc
    integer, parameter      :: tolsize = xnum*ynum  ! element count of matrix L
    real                    :: dx = xsize/(xnum-1)  ! grid step along yc
    real                    :: dy = ysize/(ynum-1)  ! grid step along xc

    
    ! define time step
    integer, parameter      :: tnum = 2E4           ! number of time step
    real, parameter         :: kappa_dt = 1.0e-6    ! just for define time step
    real                    :: dt                   ! time step
    real                    :: dtmy                 ! unit: a million year
    
    ! solidus, liquidus and melt constants (from katz, 2003)
    real, parameter         :: r0 = 0.5             ! cpx/melt
    real, parameter         :: r1 = 0.08            !cpx/melt/Gpa
    real, parameter         :: beta1 = 1.50         ! no unit
    real, parameter         :: beta2= 1.50          ! no unit
    real, parameter         :: KI = 43              ! C wt!**{-r}
    real, parameter         :: gama = 0.75          ! no unit
    real, parameter         :: D_H20 = 0.01         ! no unit
    real, parameter         :: chi1 = 12.00         ! wt!Gpa**{-lambda}
    real, parameter         :: chi2 = 1.00          ! wt!Gpa**{-1}
    real, parameter         :: lambda = 0.6
    real, parameter         :: modal = 0.17
    
    ! iterations and tolerance in Bisection method
    real, parameter         :: N0 = 1000 ! maximal number of iterations
    real, parameter         :: TOL = 0.001 ! tolerance of melt percentage
    
    ! strain rate
    real, parameter         :: sr = 1e-15

    ! mantle denisty
    real, parameter         :: rhom = 3300; ! mantle denisty [kg/m^3]

    ! radiogenic heat control: if Aro_exp = 0, the radiogenic heat stay the same with time, i.e. not decay
    real, parameter         :: Aro_exp = 0; 
    
    ! depths 
    real, parameter         :: uppercrust = 14e3 ! the depth of uppercrust:m
    real, parameter         :: moho_depth = 40e3 ! the depth of moho:m

    ! temp 
    real, parameter         :: T0 = 273.15      ! paramter: K
    real, parameter         :: Tsur = 0         ! surface temperature:C
    real, parameter         :: Tmoho = 600+T0              ! moho temperature: K
    real, parameter         :: Tm = 1350        ! LAB tempearture: K
      
    ! intrusion shape 
    real                    :: intrusion_width  ! size : the with of intrusion, which equals top and bottom. 
    real                    :: top = 100e3      ! size: the top of intrusion(m)
    real                    :: middle = 200e3   ! size: the middle of intrusion(m)
    real                    :: bottom = 100e3   ! size: the bottom of intrusion(m)

    !!! model info 
    integer                 :: model_number

    ! bulk water fraction in weight 
    real                    :: X_H20_bulk_intrusion
    real                    :: X_H20_bulk_mantle

    real                    :: intrusion_thickness          ! the thickness of intrusion(m)
    real                    :: Tp                           ! the temperature of intrusive magma: C
    integer                 :: shape = 0                      ! 1,2,3 to choose shape of intrusion, and define_shape if shape = 0
    
    !!!! namelist for input, which store in different files
    namelist /model_control/ model_number, intrusion_width, intrusion_thickness, Tp, X_H20_bulk_intrusion, X_H20_bulk_mantle
    namelist /model_comparison/ model_number, intrusion_thickness, Tp, X_H20_bulk_intrusion, X_H20_bulk_mantle

    ! comparison 
    integer                 :: comparison
    
    ! coordinate
    real                    :: xc(xnum)  ! X coordinate: m
    real                    :: yc(ynum)  ! Y coordinate: m

    ! melting parameters
    real(kind=8)            :: F                           ! melt percentage
    real(kind=8)            :: ft                          ! parameter
    real(kind=8)            :: P                           ! Pressure: Pa
    real(kind=8)            :: fx  
    real(kind=8)            :: X_H20
    real(kind=8)            :: T_solidus                   ! solidus temperature [C]
    real(kind=8)            :: T_sat                       ! saterated temperature [C]
    real(kind=8)            :: delta_T                     ! temperature difference [C]
    real(kind=8)            :: Aro
    real(kind=8)            :: kappa             
    real(kind=8)            :: T_liquidus_lherz
    real(kind=8)            :: T_liquidus
    real(kind=8)            :: thermal_conductivity
    real(kind=8)            :: ther_cond_surface
    real(kind=8)            :: X_sat
    
    
    ! initial temperature and density distrubtion 
    real(kind=8)    :: initial_temp(ynum, xnum)        ! initial temp distribution [K]
    real(kind=8)    :: no_intrusive_rho(ynum, xnum)    ! density distribution before intrusion  [kg/m^3]
    real(kind=8)    :: initial_rho(ynum, xnum)        ! density distribution when intrusion  [kg/m^3]
    real(kind=8)    :: no_intrusive_temp(ynum, xnum)   ! temp distribution before intrusion  [kg/m^3]
    real(kind=8)    :: Ts(ynum, xnum)                  ! solidus temperature: [K]
    real(kind=8)    :: Tl(ynum, xnum)                  ! liquidus temperature: [K]
    real(kind=8)    :: Tll(ynum, xnum)                 ! lerzerlite liquidus temperature: [K]
    real(kind=8)    :: rho(ynum, xnum)                 ! density: [kg/m^3]
    real(kind=8)    :: temp(ynum, xnum)                ! temperature: [K]
    real            :: sigma_d(ynum, xnum)             ! brittle strength: [pa]
    real            :: sigma_b(ynum, xnum)             ! ductile strength: [pa]
    real            :: sigma_int(xnum)                 ! strength interation: [pa m]
    real            :: sigma(ynum, xnum)               ! strength: [pa]
    real            :: heat_flux(xnum-9)               ! 
    real            :: melt_percentage(ynum, xnum)
    
    real            :: h                           ! depth: m
    real            :: PG                          ! Pressure: Gpa
    real            :: PM                          ! Pressure: Mpa
    real            :: t                           ! time [s]
    real            :: tmy                         ! time [million year]
    real(kind=8)    :: mux,muy                     ! parameters in FD
    real            :: subsidence(xnum)            ! subsidence [m]
    real            :: save_subsidence(xnum,tnum)  ! save subsidence data [m]
    real            :: save_heat_flux(xnum-9,tnum) ! save heat flux data [mw/m]
    real            :: save_sigma_int(xnum, tnum)  ! save strength integration data [N/m]
    real            :: save_sigma(ynum, tnum)      ! save sigma along center [Pa]
    real            :: save_subsidence_rate(xnum, tnum)  ! save_subsidence_rate [m/millon year]
    real            :: initial_sigma(ynum)          ! save sigma along center [Pa]
    real            :: initial_sigma_int(xnum)   ! save initial sigma intergtaion[N/m]
    real            :: initial_heat_flux(xnum)   ! save initial heat flux [mw/m]
    
    integer         :: i,j,k,kk,si              ! all dummy number
    integer         :: MI(ynum, xnum)           ! material type 
    integer         :: statCode                 ! iostat
    character(len=3):: cTemp                    ! temp variable
    
    !underplaitng thickness
    real            :: melt_sum                ! the sum of underplating melt percentage
    integer         :: melt_sum_index          ! the sum of which F > 0  for underplating
    real            :: underplating_thickness  ! the thickness of underplating [m]
    real            :: save_underplating_thickness(tnum) ! save [m]
    real            :: graintic_sum            ! the sum of underplating melt percentage
    integer         :: graintic_sum_index      ! the sum of which F > 0  fort granitic rock
    real            :: save_granitic_thickness(tnum)  ! [m]
    real            :: granitic_thickness      ! [m]
    
    
    ! paridso declare
    TYPE(MKL_PARDISO_HANDLE)    :: pt(64) !Handle to internal data structure. The entries must be set to zero prior to the first call to pardiso. 
    integer,parameter           :: sparse_size = (xnum*ynum-((xnum-2+ynum-2)*2+4))*5+xnum*2+2*2*(ynum-2) ! sparse_size = internal points * 5 + upper and lower boundary + left and right boundary*2
    integer         :: nrhs = 1 !Number of right-hand sides that need to be solved for
    integer         :: maxfct = 1, mnum = 1  !default: 1
    integer         :: n  ! the number of equations, ie. the rank of L
    integer         :: phase ! Controls the execution of the solver. Usually it is a two- or three-digit integer. The first digit indicates the starting phase of execution and the second digit indicates the ending phase. 
    integer         :: mtype = 11 ! real unsymmetric equations
    integer         :: error = 0 !initialize error flag
    integer         :: msglvl = 0 !Message level information: 1 means print statistical information
    integer         :: iparm(64) ! This array is used to pass various parameters to Intel MKL PARDISO and to return some useful information after execution of the solver.
    integer         :: ia(tolsize+1)  !For CSR3 format, ia(i) (i≤n) points to the first column index of row i in the array ja. 
    integer         :: ja(sparse_size) !For CSR3 format, array ja contains column indices of the sparse matrix A. 
    real(kind=8)    :: a(sparse_size)  !Array. Contains the non-zero elements of the coefficient matrix A corresponding to the indices in ja. 
    integer         :: perm(tolsize) ! Output: Array, size (n)
    real(kind=8)    :: b(tolsize) !Array, size (n, nrhs). On entry, contains the right-hand side vector/matrix B, which is placed in memory contiguously.
    real(kind=8)    :: x(tolsize) !Array, size (n, nrhs). On entry, contains the right-hand side vector/matrix B, which is placed in memory contiguously.
    real(kind=8)    :: ddum(1) ! just for output, no meaning
    
    ! mkl_dcsrcoo: Sparse BLAS Coordinate Matrix Storage Format
    integer         :: job(8)               
    integer         :: local
    integer         :: nnz = sparse_size  !Returns the number of converted elements of the matrix a for job(1)=0.
    integer         :: info !If info=0, the execution is successful.If info=1, the routine is interrupted because there is no space in the arrays acoo, rowind, colind according to the value nzmax.
    real(kind=8)    :: values(sparse_size)  ! none zero value
    integer         :: rows(sparse_size)    ! row index of value
    integer         :: columns(sparse_size) ! column index of value
    

contains

    subroutine is_comparison()
        
        implicit none

        if (tnum <= 1) then
            comparison = 1
            write(*,*) 'COMPARISON!  TNUM=1'
        end if
    
    end subroutine is_comparison
    
    subroutine read_model_data()

        implicit none

        read(*, nml=model_control)
        write(cTemp, '(I3)') model_number

    end subroutine read_model_data

    
    subroutine define_shape
    ! Purpose: define the shape of underplating: both on crust and upper mantle
        implicit none
        
        ! define basic shape of intrusion shape
        top = intrusion_width;
        bottom = intrusion_width;
        middle = intrusion_width + 100e3; ! transition_zone is 100km

        do j=1,xnum
            do i=1,ynum 

                if (yc(i) <= uppercrust) then 
                    MI(i,j) = 1 ! upper crust
                else if ((yc(i) <= moho_depth) .AND. (yc(i) > uppercrust)) then 
                    MI(i,j) = 2 ! lower crust
                else 
                    MI(i,j) = 3 ! mantle
                end if

                if ((yc(i) >= ((moho_depth+intrusion_thickness/2)-intrusion_thickness)) .AND. (yc(i)<= (moho_depth+intrusion_thickness/2)) &
                    .AND. (yc(i)>=(-intrusion_thickness/(middle-top)*xc(j)-1.0/2.0*(2.0*intrusion_thickness*middle-intrusion_thickness*top-intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*middle+2.0*(moho_depth+intrusion_thickness/2)*top)/(middle-top))) &
                    .AND. (yc(i)>=(intrusion_thickness/(middle-top)*xc(j)-1.0/2.0*(2.0*intrusion_thickness*middle-intrusion_thickness*top+intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*middle+2.0*(moho_depth+intrusion_thickness/2)*top)/(middle-top))) &
                    .AND. (yc(i)<=(-intrusion_thickness/(bottom-middle)*xc(j)-1.0/2.0*(intrusion_thickness*bottom-intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*bottom+2.0*(moho_depth+intrusion_thickness/2)*middle)/(bottom-middle))) &
                    .AND. (yc(i)<=(intrusion_thickness/(bottom-middle)*xc(j)-1.0/2.0*(intrusion_thickness*bottom+intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*bottom+2.0*(moho_depth+intrusion_thickness/2)*middle)/(bottom-middle)))) then
                  
                    MI(i,j) = 4 ! intrusion
                end if
                   
            end do 
        end do 
    end subroutine define_shape



    subroutine shape1()
    ! Purpose: define the shape of underplating: both on crust and upper mantle
        implicit none


        do j=1,xnum
            do i=1,ynum 

                if (yc(i) <= uppercrust) then 
                    MI(i,j) = 1 ! upper crust
                else if ((yc(i) <= moho_depth) .AND. (yc(i) > uppercrust)) then 
                    MI(i,j) = 2 ! lower crust
                else 
                    MI(i,j) = 3 ! mantle
                end if

                if ((yc(i) >= ((moho_depth+intrusion_thickness/2)-intrusion_thickness)) .AND. (yc(i)<= (moho_depth+intrusion_thickness/2)) &
                    .AND. (yc(i)>=(-intrusion_thickness/(middle-top)*xc(j)-1.0/2.0*(2.0*intrusion_thickness*middle-intrusion_thickness*top-intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*middle+2.0*(moho_depth+intrusion_thickness/2)*top)/(middle-top))) &
                    .AND. (yc(i)>=(intrusion_thickness/(middle-top)*xc(j)-1.0/2.0*(2.0*intrusion_thickness*middle-intrusion_thickness*top+intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*middle+2.0*(moho_depth+intrusion_thickness/2)*top)/(middle-top))) &
                    .AND. (yc(i)<=(-intrusion_thickness/(bottom-middle)*xc(j)-1.0/2.0*(intrusion_thickness*bottom-intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*bottom+2.0*(moho_depth+intrusion_thickness/2)*middle)/(bottom-middle))) &
                    .AND. (yc(i)<=(intrusion_thickness/(bottom-middle)*xc(j)-1.0/2.0*(intrusion_thickness*bottom+intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness/2)*bottom+2.0*(moho_depth+intrusion_thickness/2)*middle)/(bottom-middle)))) then
                  
                    MI(i,j) = 4 ! intrusion
                end if
                   
            end do 
        end do 
    end subroutine shape1


    subroutine shape2()
        ! Purpose: define the shape of underplating: upper mantle only
        implicit none

        do j=1,xnum
            do i=1,ynum 

                if (yc(i) <= uppercrust) then 
                    MI(i,j) = 1 ! upper crust
                else if ((yc(i) <= moho_depth) .AND. (yc(i) > uppercrust)) then 
                    MI(i,j) = 2 ! lower crust
                else 
                    MI(i,j) = 3 ! mantle
                end if

                if ((yc(i) >= moho_depth) .AND. (yc(i)<= (moho_depth+intrusion_thickness)) &
                    .AND. (yc(i)<=(-intrusion_thickness/(bottom-middle)*xc(j)-1.0/2.0*(intrusion_thickness*bottom-intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness)*bottom+2.0*(moho_depth+intrusion_thickness)*middle)/(bottom-middle))) &
                    .AND. (yc(i)<=(intrusion_thickness/(bottom-middle)*xc(j)-1.0/2.0*(intrusion_thickness*bottom+intrusion_thickness*xsize-2.0*(moho_depth+intrusion_thickness)*bottom+2.0*(moho_depth+intrusion_thickness)*middle)/(bottom-middle)))) then
                  
                    MI(i,j) = 4 ! intrusion 
                end if
                   
            end do 
        end do 
    end subroutine shape2

    subroutine shape3()
        ! Purpose: define the shape of underplating:  crust only
        implicit none

        do j=1,xnum
            do i=1,ynum 

                if (yc(i) <= uppercrust) then 
                    MI(i,j) = 1 ! upper crust
                else if ((yc(i) <= moho_depth) .AND. (yc(i) > uppercrust)) then 
                    MI(i,j) = 2 ! lower crust
                else 
                    MI(i,j) = 3 ! mantle
                end if

                if ((yc(i) >= (moho_depth-intrusion_thickness)) .AND. (yc(i)<= moho_depth) &
                    .AND. (yc(i)>=(-intrusion_thickness/(middle-top)*xc(j)-1.0/2.0*(2*intrusion_thickness*middle-intrusion_thickness*top-intrusion_thickness*xsize-2.0*moho_depth*middle+2.0*moho_depth*top)/(middle-top))) &
                    .AND. (yc(i)>=(intrusion_thickness/(middle-top)*xc(j)-1.0/2.0*(2*intrusion_thickness*middle-intrusion_thickness*top+intrusion_thickness*xsize-2.0*moho_depth*middle+2.0*moho_depth*top)/(middle-top)))) then
                  
                    MI(i,j) = 4 ! intrusion 
                end if
                   
            end do 
        end do 
    end subroutine shape3

    subroutine write_data_file()
        
        implicit none
        ! open file
        !$omp parallel
        !$omp sections
        !$omp section 
        open(unit=21, file='MODEL'//trim(adjustl(cTemp))//'_'//'initial_temp.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=22, file='MODEL'//trim(adjustl(cTemp))//'_'//'x_coordinate.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=221, file='MODEL'//trim(adjustl(cTemp))//'_'//'y_coordinate.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=23, file='MODEL'//trim(adjustl(cTemp))//'_'//'solidus.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=24, file='MODEL'//trim(adjustl(cTemp))//'_'//'liquidus.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=25, file='MODEL'//trim(adjustl(cTemp))//'_'//'lherz_liquidus.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=26, file='MODEL'//trim(adjustl(cTemp))//'_'//'material_type.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=27, file='MODEL'//trim(adjustl(cTemp))//'_'//'temp2D.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=28, file='MODEL'//trim(adjustl(cTemp))//'_'//'b.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=29, file='MODEL'//trim(adjustl(cTemp))//'_'//'a.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=30, file='MODEL'//trim(adjustl(cTemp))//'_'//'initial_density.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=31, file='MODEL'//trim(adjustl(cTemp))//'_'//'melt_percentage.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=32, file='MODEL'//trim(adjustl(cTemp))//'_'//'subsidence.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=33, file='MODEL'//trim(adjustl(cTemp))//'_'//'heat_flux.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=34, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_sigma.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=35, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_sigma_int.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=36, file='MODEL'//trim(adjustl(cTemp))//'_'//'initial_sigma.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=37, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_granitic_thickness.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=38, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_underplating_thickness.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=39, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_subsidence_rate.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=40, file='MODEL'//trim(adjustl(cTemp))//'_'//'no_intrusive_rho.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp section 
        open(unit=41, file='MODEL'//trim(adjustl(cTemp))//'_'//'no_intrusive_temp.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO OPEN Error'
        !$omp end sections
        !$omp end parallel
        
        
        ! wrtie initial temp
        !$omp parallel
        !$omp sections private(i,j) 
        do i = 1, ynum
            do j = 1, xnum
                write(21,'(1x,f,$)') initial_temp(i,j)
            end do
            write(21,*) ' '
        end do
        close(21)
        
        ! write xc coordinate
        !$omp section 
        do i = 1, xnum
            write(22, '(1x,f,$)') xc(i)
        end do
        write(22,*) ' '
        close(22)
        
        ! write yc coordinate
        !$omp section 
        do j = 1, ynum
            write(221, '(1x,f,$)') yc(j)
        end do
        write(221,*) ' '
        close(221)
        
        ! write solidus
        !$omp section 
        do i = 1, ynum
            do j = 1, xnum
                write(23, '(1x,f,$)') Ts(i,j)
            end do
            write(23,*) ' '
        end do
        close(23)
        
        ! write  liquidus
        !$omp section 
        do i = 1, ynum
            do j = 1, xnum
                write(24, '(1x,f,$)') Tl(i,j)
            end do
            write(24,*) ' '
        end do
        close(24)
        
        ! write  liquidus
        !$omp section 
        do i = 1, ynum
            do j = 1, xnum
                write(25, '(1x,f,$)') Tll(i,j)
            end do
            write(25,*) ' '
        end do
        close(25)
        
        ! material type
        !$omp section 
        do i = 1, ynum
            do j = 1, xnum
                write(26, '(1x,I0,$)') MI(i,j)
            end do
            write(26,*) ' '
        end do
        close(26)
        
        ! wrtie current temp
        !$omp section 
        do i = 1, ynum
            do j = 1, xnum
                write(27,'(1x,E,$)') temp(i,j)
            end do
            write(27,*) ' '
        end do
        close(27)
        
        
        ! write array b
        !$omp section 
        do i = 1,tolsize
            write(28,'(1x,E,$)') b(i)
        end do
        close(28)
        
        
        ! write array a
        !$omp section 
        do i = 1,sparse_size
            write(29,'(1x,E,$)') a(i)
        end do
        close(29)
        
        ! write  initial density
        !$omp section 
        do i = 1,ynum
            do j = 1,xnum
                write(30,'(1x,E,$)') initial_rho(i,j)
            end do
            write(30,*) ' '
        end do
        close(30)
        
        ! write  initial density
        !$omp section 
        do i = 1,ynum
            do j = 1,xnum
                write(31,'(1x,f,$)') melt_percentage(i,j)
            end do
            write(31,*) ' '
        end do
        close(31)
        
        ! write subsidence
        !$omp section 
        do j = 1, tnum
            do i = 1, xnum
                write(32, '(1x,f,$)') save_subsidence(i,j)
            end do
            write(32,*) ' '
        end do
        close(32)
        
        ! write heat flux
        !$omp section 
        do j = 1, tnum
            do i = 1, xnum-9
                write(33, '(1x,f,$)') save_heat_flux(i,j)
            end do
            write(33,*) ' '
        end do
        close(33)
        
        ! write save_sigma
        !$omp section 
        do j = 1, tnum
            do i = 1, ynum
                write(34, '(1x,f,$)') save_sigma(i,j)
            end do
            write(34,*) ' '
        end do
        close(34)
        
        ! write save_sigma_int
        !$omp section 
        do j = 1, tnum
            do i = 1, xnum
                write(35, '(1x,E,$)') save_sigma_int(i,j)
            end do
            write(35,*) ' '
        end do
        close(35)
        
        ! write initial sigma
        !$omp section 
        do j = 1, ynum
            write(36, '(1x,E,$)') initial_sigma(j)
        end do
        close(36)
        
        
        ! save granitic thickness
        !$omp section 
        do k = 1, tnum
            write(37, '(1x,E,$)') save_granitic_thickness(k)
        end do
        close(37)
        
        ! write underplating thickness
        !$omp section 
        do k = 1, tnum
            write(38, '(1x,f,$)') save_underplating_thickness(k)
        end do
        close(38)

        ! write subsidence rate
        !$omp section 
        do j = 1, tnum
            do i = 1, xnum
                write(39, '(1x,f,$)') save_subsidence_rate(i,j)
            end do
            write(39,*) ' '
        end do
        close(39)
        
        ! write intrusive rho
        !$omp section 
        do i = 1,ynum
            do j = 1,xnum
                write(40,'(1x,E,$)') no_intrusive_rho(i,j)
            end do
            write(40,*) ' '
        end do
        close(40)
        
        ! write intrusive temp
        !$omp section 
        do i = 1,ynum
            do j = 1,xnum
                write(41,'(1x,E,$)') no_intrusive_temp(i,j)
            end do
            write(41,*) ' '
        end do
        close(41)
        !$omp end sections

        !$omp end parallel
    
    end subroutine write_data_file

    subroutine write_thickness_file()

        implicit none
        
        ! open file to write
        if (model_number==1) then
            open(unit=138, file='comparison_underplating_thickness.output', status='new', iostat=statCode); if (statCode /= 0) print *, 'THINKNESS FILE IO OPEN Error'
        else
            open(unit=138, file='comparison_underplating_thickness.output', status='old', position='append', iostat=statCode); if (statCode /= 0) print *, 'THINKNESS FILE IO OPEN Error'
        end if       

        ! write model number
        write(138, '(1x,I0,$)') model_number
        
        ! write underplating thickness
        write(138, '(1x,f,$)') save_underplating_thickness(1)
            
        ! write granitic thickness
        write(138, '(1x,f, $)') MAXVAL(save_granitic_thickness)
        
        ! write melt average of intrusion
        write(138, '(1x,f)') melt_sum
        
        ! close file
        close(138)

    end subroutine write_thickness_file

    subroutine melt_percentage_katz(P, T, X_H20_bulk, modal, TOL, ft, F)

        implicit none 

        ! Arguments declarations 
        real(kind=8), intent(in)    :: P  ! pressure [pa]
        real(kind=8), intent(in)    :: T  ! temperature [C]
        real, intent(in)            :: X_H20_bulk  ! weight percentage of water in mantle: 0 ~ 1
        real,intent(in)             :: modal  ! modal parameter for mantle: 0~1
        real, intent(in)            :: TOL  ! tolerace for iteration, default = 0.001
        real(kind=8), intent(out)   :: ft  ! parameter for FD 
        real(kind=8), intent(out)   :: F  ! melt percentage

        ! Variable declarations 
        real(kind=8) :: delta_T  ! the temperature difference between dyhydrate and water in solidus and liquidus
        real         :: PG  ! pressure [Gpa]
        real(kind=8) :: F0  ! initial guess of F
        real(kind=8) :: F1  ! initial guess of F 
        real(kind=8) :: F_cpx_out  ! when F > F_cpx_out, opx starts to melt
        real(kind=8) :: FA  ! start value for bisection algorithm
        real(kind=8) :: FP  ! end value for bisection algorithm
        integer      :: i  ! dummy number
        real(kind=8) :: R_cpx  ! 
        real(kind=8) :: T_cpx_out  ! the temperature when F = F_cpx_out  
        real(kind=8) :: T_liquidus  ! liquidus temperature
        real(kind=8) :: T_liquidus_lherz  ! liquidus temperature of lherzolite 
        real(kind=8) :: T_solidus  ! solidus temperature
        real(kind=8) :: X_sat  ! 
        

        ! Pressure, GPa 
        PG = P/1.0e9

        ! liquidus and solidus 
        T_solidus = 1085.7+132.9*PG-5.1*PG**2.0 !C
        T_liquidus_lherz = 1475.0+80.0*PG-3.2*PG**2.0 !C
        T_liquidus = 1780+45.0*PG-2.0*PG**2.0 !C

        ! F cpx 
        R_cpx = 0.5 + 0.08*PG
        F_cpx_out = modal/R_cpx

        ! cal saturation 
        X_sat = (12.00*PG**(0.6)+1.00*PG)

        ! determing F when cpx 
        F0 = 0.00 ! initial guess for F
        F1 = 1.00 ! initial guess for F
        i = 1
        FA = F0 - ((T-T_solidus+43.0*(X_H20_bulk/(0.01+F0*(1.0-0.01)))**0.75)/(T_liquidus_lherz-T_solidus))**1.50

        do while (i <= 1000) 
            F = (F0 + F1)/2
            FP = F - ((T-T_solidus+43*(X_H20_bulk/(0.01+F*(1-0.01)))**0.75)/(T_liquidus_lherz-T_solidus))**1.50
            if ((FP == 0) .or. (abs(F0-F1) < TOL)) then 
                exit
            end if 
            i = i + 1
            if (FA*FP > 0) then 
                F0 = F
                FA = FP
            else
                F1 = F
            end if 
        end do 


        ! determing F when opx 
        if (F > F_cpx_out) then 

            F0 = 0.00 ! initial guess for F
            F1 = 1.00 ! initial guess for F
            i = 1
            delta_T = 43.0*(X_H20_bulk/(0.01+F0*(1.0-0.01)))**0.75
            T_cpx_out = F_cpx_out**(1.0/1.50)*(T_liquidus_lherz-T_solidus)+T_solidus
            FA = F0 - F_cpx_out - (1.0-F_cpx_out)*((T-T_cpx_out+delta_T)/(T_liquidus-T_cpx_out))**1.5
            
            do 

                F = (F0 + F1)/2.0
                delta_T = 43.0*(X_H20_bulk/(0.01+F*(1.0-0.01)))**0.75
                T_cpx_out = F_cpx_out**(1.0/1.50)*(T_liquidus_lherz-T_solidus)+T_solidus
                FP = F - F_cpx_out - (1.0-F_cpx_out)*((T-T_cpx_out+delta_T)/(T_liquidus-T_cpx_out))**1.5
                if ((i>1000) .or. (FP == 0) .or. (abs(F0-F1) < TOL)) then 
                    exit
                end if 

                i = i+1
                if (FA*FP > 0) then 
                    F0 = F
                    FA = FP
                else
                    F1 = F
                end if 

            end do 
        end if 


        ! determing ft and F 
        if (X_H20_bulk/(0.01+F*(1-0.01)) >= X_sat) then 

            if (T < (T_solidus+43.0*(X_sat)**0.75)) then ! if T < Tsolidus
                F = 0
            else
                F = ((T-T_solidus+43.0*(X_sat)**0.75)/(T_liquidus_lherz-T_solidus))**1.50
            end if 
            ft = 1.50*F**((1.50-1.0)/1.50)/(T_liquidus_lherz-T_solidus)
        else if (F < F_cpx_out) then 
            ft = 1.50*F**((1.50-1.0)/1.50)/(T_liquidus_lherz-T_solidus)
        else if (F > F_cpx_out) then 
            ft = (1.0-F_cpx_out)*1.5*((T-T_cpx_out+43.0*(X_H20_bulk/(0.01+F*(1.0-0.01))))/(T_liquidus-T_cpx_out))**(1.5-1.0)/(T_liquidus-T_cpx_out)
            if (ISNAN(ft)) then
                ft = 0
            end if
        end if

        ! special condition (when F reaches 1) 
        if ((1-F) < TOL) then 
            F = 1 ! tolerance
            ft = 0 ! no increase any more when F = 1
        end if 
        
        return
    end subroutine melt_percentage_katz 
    


end module model_mod


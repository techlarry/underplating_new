program model
 
!author: wangzhenhua 
!title: thermal evolution 

! Using intel ifort MKL to compile
use model_mod

!Variable declarations 
implicit none

! tnum < 1000 if comparison
CALL is_comparison()

! read model data
CALL read_model_data()

! calculate coordinate and time info
xc = [((i*dx),i=0,xnum-1)]             ! xc coordinate [m]
yc = [((i*dy),i=0,ynum-1)]             ! yc coordinate [m]
dt = min(dx,dy)**2.0/3.0/kappa_dt*0.75        ! time interval [s]
dtmy = dt/(1.0e6*365.25*24*3600)      ! time interval [million year]

! select shape according model data from read_model_data
select case (shape)
    
    case(1)

        CALL shape1()
    
    case(2)

        CALL shape2()

    case(3)

        CALL shape3()
    
    case default

        CALL define_shape() 

end select       




!--------------------------------------- set initial/no_intrusive temperature and density, solidus, liquidus, field strength, surface heat-----------------------------------------
! nodes cycle
!$omp parallel do private(j, h, P, PG, PM, F, X_H20, X_sat, delta_T, T_solidus, T_sat, T_liquidus_lherz, T_liquidus)
do i=1,ynum        
    do j=1,xnum

        h = (i-1)*dy ! depth: m
        P = h/28.0e3*1.0e9 !pressure: Pa
        PG = h/28.0e3 !pressure: GPa
        PM = h/28.0e3*1e3 !pressure: MPa

        ! initial temperature: K
        initial_temp(i, j) = yc(i)/ysize*Tm + T0
        
        ! no intrusive temperature: K
        no_intrusive_temp(i, j)  = yc(i)/ysize*Tm + T0
        
        !! no intrusive property: define temperature and density before instrusion
        if (yc(i) <= uppercrust) then ! upper crust
            no_intrusive_rho(i,j) = 2800.0*(1.0-thermal_expansion*(no_intrusive_temp(i,j)-298.0))*(1.0+thermal_compressibility*(P-1e5)) ! upper crust density:kg/m^3
        else if ((yc(i) <= moho_depth) .and. (yc(i) > uppercrust)) then ! lower crust
            no_intrusive_rho(i,j) = 2900.*(1.-thermal_expansion*(no_intrusive_temp(i,j)-298))*(1.+thermal_compressibility*(P-1e5)) ! lower crust density:kg/m^3
        else ! mantle
            no_intrusive_rho(i,j) = 3300.*(1.-thermal_expansion*(no_intrusive_temp(i,j)-298))*(1.+thermal_compressibility*(P-1e5)) ! mantle density:kg/m^3
        end if

        
        select case (MI(i,j))

            case (1)  ! upper crust

                if (P < 1200e6) then 
                    Ts(i,j) = 889 + 17900/(P/1e6+54) + 20200/(P/1e6+54)**2
                else
                    Ts(i,j) = 831 + 0.06*P/1e6
                end if 
                Tl(i,j) = 1262 + 0.09*P/1e6

                initial_rho(i,j)  = 2800*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! upper crust density:kg/m**3


            case (2,5)  ! lower crust

                if (P < 1600e6) then 
                    Ts(i,j) = 973-70400/(P/1e6+354) + 77800000/(P/1e6+354)**2
                else
                    Ts(i,j)= 935 + 0.0035*P/1e6 + 0.0000062*(P/1e6)**2
                end if 
                Tl(i,j) = 1423 + 0.105*P/1e6

                initial_rho(i,j)    = 2900*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! lower crust density:kg/m**3
                if (initial_temp(i,j) > Ts(i,j)) then
                    melt_percentage(i,j) = (initial_temp(i,j)-Ts(i,j))/(Tl(i,j)-Ts(i,j))
                    MI(i,j) = 5
                    initial_rho(i,j)    = (2800*melt_percentage(i,j)+2900*(1-melt_percentage(i,j)))*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! melt density:kg/m**3
                end if


                
            case (3,6) ! mantle
                

                ! Solidus Temperature 
                F = 0 ! no melt
                X_H20 = X_H20_bulk_mantle/(D_H20+F*(1-D_H20)) ! dissovled water fraction in the melt
                X_sat = (chi1*PG**(lambda)+chi2*PG)
                delta_T = KI*X_H20**(gama) ! C
                T_solidus = 1085.7+132.9*PG-5.1*PG**2-delta_T !C
                T_sat = 1085.7+132.9*PG-5.1*PG**2 - KI*(X_sat)**(gama) !C
                if (X_H20 < X_sat) then
                    Ts(i,j) = T_solidus + T0
                else
                    Ts(i,j) = T_sat + T0
                end if 

                ! liquidus Temperature 
                F = 1.0 ! complete melt
                X_H20 = X_H20_bulk_mantle/(D_H20+F*(1-D_H20)) ! dissovled water fraction in the melt
                X_sat = (chi1*PG**(lambda)+chi2*PG)
                delta_T = KI*X_H20**(gama) ! C
                T_liquidus_lherz = 1475.0+80.0*PG-3.2*PG**2-delta_T !C
                T_liquidus = 1780+45*PG-2*PG**2-delta_T !C
                T_sat = 1085.7+132.9*PG-5.1*PG**2 - KI*(X_sat)**(gama) !C
                if (X_H20 < X_sat) then 
                    Tl(i,j) = T_liquidus + T0 !K
                    Tll(i,j) = T_liquidus_lherz + T0
                else
                    Tl(i,j) = T_sat + T0 !K
                    Tll(i,j) = T_sat + T0 !K
                end if 

                initial_rho(i,j)    = 3300*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! mantle density:kg/m**3
                call melt_percentage_katz(P, initial_temp(i,j)-T0, X_H20_bulk_mantle, modal, TOL, ft, F) ! F is melt percentage(0-1), ft is parameter
                ! set melted mantle 
                melt_percentage(i,j) = F
                if (F > TOL) then
                    MI(i,j) = 6
                    initial_rho(i,j)   = (2700*melt_percentage(i,j)+3300*(1-melt_percentage(i,j)))*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! melt density:kg/m**3
                end if



            case (4,7) ! intrusive magma 

                initial_temp(i, j) = Tp + T0 ! intrusive magma temperature(the same): K

                ! Solidus Temperature 
                F = 0 ! no melt
                X_H20 = X_H20_bulk_intrusion/(D_H20+F*(1-D_H20)) ! dissovled water fraction in the melt
                X_sat = (chi1*PG**(lambda)+chi2*PG)
                delta_T = KI*X_H20**(gama) ! C
                T_solidus = 1085.7+132.9*PG-5.1*PG**2-delta_T !C
                T_sat = 1085.7+132.9*PG-5.1*PG**2 - KI*(X_sat)**(gama) !C
                if (X_H20 < X_sat) then 
                    Ts(i,j) = T_solidus + T0 !K
                else
                    Ts(i,j) = T_sat + T0 !K
                end if 

                ! liquidus Temperature 
                F = 1.0 ! complete melt
                X_H20 = X_H20_bulk_intrusion/(D_H20+F*(1-D_H20)) ! dissovled water fraction in the melt
                X_sat = (chi1*PG**(lambda)+chi2*PG)
                delta_T = KI*X_H20**(gama) ! C
                T_liquidus = 1780+45*PG-2*PG**2-delta_T !C
                T_liquidus_lherz = 1475.0+80.0*PG-3.2*PG**2-delta_T !C
                T_sat = 1085.7+132.9*PG-5.1*PG**2 - KI*(X_sat)**(gama) !C
                if (X_H20 < X_sat) then 
                    Tl(i,j) = T_liquidus + T0 !K
                    Tll(i,j) = T_liquidus_lherz + T0
                else
                    Tl(i,j) = T_sat + T0 !K
                    Tll(i,j) = T_sat + T0 !K
                end if


                initial_rho(i,j)    = 3100*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! intrusive magma density:kg/m**3
                call melt_percentage_katz(P, initial_temp(i,j)-T0, X_H20_bulk_intrusion, modal, TOL, ft, F) ! F is melt percentage(0-1), ft is parameter
                melt_percentage(i,j) = F
                ! set melted underplating material
                if (F > TOL) then
                    MI(i,j) = 7
                    initial_rho(i,j)   = (2900*melt_percentage(i,j)+3100*(1-melt_percentage(i,j)))*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! melt density:kg/m**3
                end if


            case default

                write(*,*) "Material_type is wrong"

            end select

        ! calculating brittle strength(MPa)
        if (PM <= 120) then
            sigma_b(i,j) = 0.85*PM        ! brittle strength(MPa)
        else
            sigma_b(i,j) = 0.5*PM+60   ! brittle strength(Mpa)
        end if
        
        ! calculating ductile strength(MPa)
        if  (yc(i) < uppercrust) then      ! upper crust
            sigma_d(i,j) = (sr/10**(-3.5))**(1.0/2.3)*exp(154.0e3/Rgas/no_intrusive_temp(i,j)/2.3)! ductile strength(MPa)
        else if ((yc(i) <= moho_depth) .and. ( yc(i) > uppercrust))  then     ! lower crust
            sigma_d(i,j) = (sr/10**(-3.5))**(1.0/3.2)*exp(238.0e3/Rgas/no_intrusive_temp(i,j)/3.2)!ductile strength(MPa)
        else ! mantle
            sigma_d(i,j) = (sr/10**(4.4))**(1.0/3.5)*exp(532.0e3/Rgas/no_intrusive_temp(i,j)/3.5)
        end if
        
    end do
end do

! determining intial field strength i.e. 0 m.y.(Note : no intrusive magma!!!! )
sigma = min(sigma_b, sigma_d)
initial_sigma = sigma(:,xnum/2)
initial_sigma_int = sum(sigma,1)*dy ! integration(N/M)

! determing initial surface heat flux
!$omp parallel do private(ther_cond_surface)
do j = 1, xnum 
    ther_cond_surface = (0.64+807/(initial_temp(2,j)+77))*exp(0.00004*0/1e6) ! thermal conductivity [W/m/K]
    initial_heat_flux(j) = ther_cond_surface*(initial_temp(3,j)-initial_temp(1,j))/dy/2*1000 ! heat flux [mW/m^2]
end do


!-----------------------------------------------------------------------Time Loop-------------------------------------------------------------------------------------------
!! Using Finte Difference to determine temperature
! begin time simulation
t = 0                               ! total time of simulation: s
temp = initial_temp                 ! temperature of upwelling mantle: K
rho = initial_rho                   ! density distribution: kg/m**3

! open file to write
if (comparison == 0) then
    open(unit=221, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_temp.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO 221 OPEN Error'
    open(unit=222, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_density.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO 222 OPEN Error'
    open(unit=223, file='MODEL'//trim(adjustl(cTemp))//'_'//'save_melt.dat', status='replace',iostat=statCode); if (statCode /= 0) print *, 'IO 223 OPEN Error'
end if

time_loop:do k=1,tnum

    t = t + dt                      ! time: second   
    tmy = t/(1e6*365.25*24*3600)    ! time: million year
    a = 0                           ! initialize a
    b = 0.0                         ! initialize b
    
    melt_percentage = 0.0           ! initialize 
    heat_flux = 0                   ! initialize
    subsidence = 0                  ! initialize
    sigma = 0                       ! initialize
    sigma_b = 0                     ! initialize
    sigma_d = 0                     ! initialize
    si = 1                          ! initialize: sparse martix index
    ia = 1                          ! initialize
    ja = 0                          ! initialize


    ! Setup PARDISO control parameter
    iparm = 0
    iparm(27) = 1 ! Matrix checker
    iparm(7) = 1 ! Number of iterative refinement steps performed.
    iparm(8) = 10 ! Iterative refinement step.

    ! Initialize the internal solver memory pointer.
    do i = 1, 64
        pt(i)%DUMMY = 0
    end do
    n = tolsize
    
    ! iteration
    time_y_loop:do i=1,ynum         
        time_x_loop:do j=1,xnum 

            kk = ynum*(j-1) + i     ! index for nodes at (i,j)
            h = (i-1)*dy            ! depth: m
            P = h/28.0e3*1.0e9      ! pressure: Pa
            
            ! applying boundary condition 
            boundary_internal_if:if ((i == 1) .or. (j == 1) .or. (i == ynum) .or. (j == xnum)) then ! four boundaries
                
                if  ((i == 1) .or. (i == ynum)) then ! left and right boundary: constant temperature condition

                    values(si) = 1
                    rows(si) = kk
                    columns(si) = kk
                    si = si + 1 
                    
                    b(kk) = initial_temp(i,j)   ! K

                else if (j == 1) then        ! right boundary: symmetric boundry
                    
                    values(si) = 1
                    rows(si) = kk
                    columns(si) = kk
                    si = si + 1

                    values(si) = -1
                    rows(si) = kk
                    columns(si) = kk+ynum
                    si = si + 1

                    b(kk) = 0
                
                else if (j == xnum) then    ! left boundary: symmetric boundry
                    
                    values(si) = -1
                    rows(si) = kk
                    columns(si) = kk-ynum
                    si = si + 1

                    values(si) = 1
                    rows(si) = kk
                    columns(si) = kk
                    si = si + 1

                    b(kk) = 0
                
                end if 

            ! internal nodes 
            else

                ! determing Aro, ft, thermal conductivity and melt percentage 
                select case (MI(i,j))

                    case (1) ! upper crust
    
                        Aro = 1.0e-6*exp(Aro_exp*tmy) ! crust temperature: volumetric radioactive heating: W/m**3(taras, 2007)
                        thermal_conductivity = (0.64+807.0/(temp(i,j)+77.0))*exp(0.00004*P/1.0e6) ! thermal conductivity: W/m/K
                        ft = 1.0/(Tl(i,j)-Ts(i,j))
    
                    case (2,5) ! lower crust
    
                        Aro = 0.25e-6*exp(Aro_exp*tmy) ! crust temperature: volumetric radioactive heating: W/m**3(taras, 2007)
                        thermal_conductivity = (1.18+474.0/(temp(i,j)+77.0))*exp(0.00004*P/1.0e6) ! thermal conductivity: W/m/K
                        ft = 1.0/(Tl(i,j)-Ts(i,j))
                        ! set melted crust
                        if (temp(i,j) > Ts(i,j)) then
                            melt_percentage(i,j) = (temp(i,j)-Ts(i,j))/(Tl(i,j)-Ts(i,j))
                            MI(i,j) = 5
                        end if
    
                    case (3,6) ! lower crust

                        Aro = 0.022e-6*exp(Aro_exp*tmy) ! mantle temperature: volumetric radioactive heating: W/m**3(taras, 2007)
                        thermal_conductivity = (0.73+1293.0/(temp(i,j)+77.0))*exp(0.00004*P/1.0e6) ! thermal conductivity: W/m/K
                        call melt_percentage_katz(P, temp(i,j)-T0, X_H20_bulk_mantle, modal, TOL, ft, F) ! F is melt percentage(0-1), ft is parameter
                        ! set melted mantle 
                        melt_percentage(i,j) = F
                        if (F > TOL) then
                            MI(i,j) = 6
                        end if
                    
                    case (4,7) ! magmatic intrusion
              
                        Aro = 0.022e-6*exp(Aro_exp*tmy) ! intrusive magma: volumetric radioactive heating: W/m**3(taras, 2007)
                        thermal_conductivity = (0.73+1293.0/(temp(i,j)+77.0))*exp(0.00004*P/1.0e6) ! thermal conductivity: W/m/K
                        call melt_percentage_katz(P, temp(i,j)-T0, X_H20_bulk_intrusion, modal, TOL, ft, F) ! F is melt percentage(0-1), ft is parameter
                        melt_percentage(i,j) = F
                        ! set melted underplating material
                        if (F > TOL) then
                            MI(i,j) = 7
                        end if

                    case default

                        write(*,*) "Material_type is wrong"

                end select 
                
                fx = 1.0 + La/Cp*ft
                kappa = thermal_conductivity/rho(i,j)/Cp ! thermal diffusivity
                mux = kappa*dt/dy**2.0 ! coefficient
                muy = kappa*dt/dx**2.0 ! coefficient
                    

                values(si) = -1.0/2.0*muy 
                rows(si) = kk
                columns(si) = kk - ynum
                si = si + 1

                values(si) = -1.0/2.0*mux
                rows(si) = kk
                columns(si) = kk - 1
                si = si + 1

                values(si) = fx + mux + muy 
                rows(si) = kk
                columns(si) = kk
                si = si + 1

                values(si) = -1.0/2.0*mux
                rows(si) = kk
                columns(si) = kk + 1
                si = si + 1

                values(si) = -1.0/2.0*muy 
                rows(si) = kk
                columns(si) = kk + ynum
                si = si + 1

                    
                b(kk) = 1.0/2.0*mux*(temp(i,j+1)+temp(i,j-1))+1.0/2.0*muy*(temp(i-1,j)+temp(i+1,j))+(fx-mux-muy)*temp(i,j)+Aro*exp(-yc(i)/d)/rho(i,j)/Cp*dt
            
            end if boundary_internal_if

        end do time_x_loop
    end do time_y_loop
   
    !----------------------------------------------------- solve equations ----------------------------------------------------------------------
    ! the matrix in the coordinate format is converted to the CSR format
    job(1) = 2 !the matrix in the coordinate format is converted to the CSR format, and the column indices in CSR representation are sorted in the increasing order within each row.
    job(2) = 1 ! one-based indexing for the matrix in CSR format is used.
    job(3) = 1 ! one-based indexing for the matrix in coordinate format is used. 
    job(4) = local ! no meaning
    job(5) = 1E6 ! maximum number of the non-zero elements allowed if job(1)=0.
    job(6) = 0 ! all arrays a, ja, ia are filled in for the output storage.
    CALL mkl_dcsrcoo(job, n, a, ja, ia, nnz, values, rows, columns, info)
    if (info /= 0) then
        print *, 'mkl_dcsrcoo error!!!!'
    end if
   
    
    !solve euqations
    ! analysis->numerical factorization->solve->iterative refinement
    phase = 13
    CALL pardiso(pt, maxfct, mnum, mtype, phase, n, a, ia, ja, perm, nrhs, iparm, msglvl, b, x, error)
    if (error /= 0) then
        print *, 'pardiso solve error!!!!'
    end if
    
    !Termination and release of memory
    phase = -1 ! release internal memory
    CALL pardiso(pt, maxfct, mnum, mtype, phase, n, ddum, perm, perm, perm, nrhs, iparm, msglvl, ddum, ddum, error)
    if (error /= 0) then
        print *, 'pardiso release memory error!!!!'
    end if
    
    !print peak memory in order to check memory
    !print *,'peak memory=', max(iparm(15), iparm(16)+iparm(17)),'kilobyte'

    ! reshape
    temp = reshape(source = x, shape = [ ynum,xnum ])

    


    !------------------------------------------ subsidence, field strength, melt percentage and thickness-------------------------------------------
    ! melt percentage and thickness
    melt_sum = 0
    melt_sum_index = 0
    graintic_sum = 0
    graintic_sum_index = 0
    granitic_thickness = 0
    
    !$omp parallel do private(j,h,P,PM) reduction( + : melt_sum_index, melt_sum, graintic_sum_index, graintic_sum)
    do i = 1,ynum 
        do j = 1,xnum 

            !!! melt percentage and thickness
            ! underplating
            if (( MI(i,j)==4).or.(MI(i,j)==7)) then
            
                melt_sum_index = melt_sum_index + 1
                melt_sum = melt_sum + melt_percentage(i,j)

            ! lower crust melt
            elseif (MI(i,j) == 5) then

                graintic_sum_index = graintic_sum_index + 1
                graintic_sum = graintic_sum + melt_percentage(i,j)

            end if

            !!! calculating density
            h = (i-1)*dy ! depth: m
            P = h/28.0e3*1.0e9 !pressure: Pa

            select case (MI(i,j))
    
                case (1) ! upper crust
                    rho(i,j)    = 2800*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! upper crust density:kg/m**3
                case (2) ! lower crust
                    rho(i,j)    = 2900*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! lower crust density:kg/m**3
                case (5)! melt lower crust
                    rho(i,j)    = (2800*melt_percentage(i,j)+2900*(1-melt_percentage(i,j)))*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! melt density:kg/m**3
                case (3) ! upper mantle
                    rho(i,j)    = 3300*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! mantle density:kg/m**3
                case (6) ! melt upper mantle
                     rho(i,j)   = (2700*melt_percentage(i,j)+3300*(1-melt_percentage(i,j)))*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! melt density:kg/m**3
                case (4) ! underplating
                    rho(i,j)    = 3100*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! intrusive magma density:kg/m**3
                case (7) ! melt underplating
                    rho(i,j)    = (2900*melt_percentage(i,j)+3100*melt_percentage(i,j))*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)) ! melt density:kg/m**3
                case default
                    write(*,*) "Material_type is wrong"
            end select 

            !!! calculating brittle strength(MPa) 
            PM = h/28.0e3*1.0e3 ! pressure: MPa
            if (PM <= 120) then 
                sigma_b(i,j) = 0.85*PM  ! MPa
            else
                sigma_b(i,j) = 0.50*PM+60  ! Mpa
            end if 

            !!! calculating ductile strength(MPa)
            select case (MI(i,j)) 
            case (1) ! upper crust
                sigma_d(i,j) = (sr/10**(-3.5))**(1.0/2.3)*exp(154.0e3/Rgas/temp(i,j)/2.3) ! ductile strength(MPa)
            case (2,5) ! lower crust
                sigma_d(i,j) = (sr/10**(-3.5))**(1.0/3.2)*exp(238.0e3/Rgas/temp(i,j)/3.2) ! ductile strength(MPa)
            case (3,6)! upper mantle
                sigma_d(i,j) = (sr/10.0**(4.4))**(1.0/3.5)*exp(532.0e3/Rgas/temp(i,j)/3.5)
            case (4,7) !  underlating
                sigma_d(i,j) = (sr/10.0**(3.3))**(1.0/4.0)*exp(470.0e3/Rgas/temp(i,j)/4.0)
            case default
                write(*,*) "Material_type is wrong"            
            end select
        
        end do 
    end do 
   
    !$omp parallel
    !$omp sections

    !thickness info 
    !$omp section
    underplating_thickness = melt_sum/(melt_sum_index)*intrusion_thickness ! the thinkness of underplating
    melt_sum = melt_sum/melt_sum_index ! the percentage of melt for underplating
    
    if  ((graintic_sum*dx*dy/graintic_sum_index/(top/2+bottom/2)) > granitic_thickness) then
        granitic_thickness = graintic_sum*dx*dy/graintic_sum_index/(top/2+bottom/2) ! the thinkness of granitic
    end if
    
    save_underplating_thickness(k) = underplating_thickness
    save_granitic_thickness(k) = granitic_thickness
    
    !subsidence info
    !$omp section
    subsidence = 1.0/rhom*(sum(no_intrusive_rho,DIM=1) - sum(rho,DIM=1))*dy ! integration(m)
    save_subsidence(:, k) = subsidence ! save data;
    ! save data, subsidence rate
    if (k > 3) then
        save_subsidence_rate(:, k) = (save_subsidence(:,k) - save_subsidence(:, k-3))/dt/3; 
    end if


    ! field strength: choose the smaller one
    !$omp section
    sigma = min(sigma_b, sigma_d)
    sigma_int = sum(sigma,1)*dy ! integration(N/M)
    save_sigma_int(:, k) = sigma_int
    save_sigma(:,k) = sigma(:,xnum/2)

    ! surface heat flux
    !$omp section 
    do j = 1,xnum ! avoiding boundary infulence,1 
        ther_cond_surface = (0.64+807/(temp(2,j)+77))*exp(0.00004*0/1e6) ! thermal conductivity: W/m/K
        heat_flux(j) = ther_cond_surface*(temp(3,j)-temp(1,j))/dy/2*1000 ! heat flux: mW/m**2
    end do 
    ! save data 
    save_heat_flux(:, k) = heat_flux

    ! print current time
    !$omp section
    write(*,'("MODEL", I0, 2x, "Current Time(step=", I0, ") =", x, f6.2, "Ma")') model_number, k, tmy

    !$omp end sections
    !$omp end parallel
    !write_file:if ((mod(k,20) == 0) .AND. (comparison == 0)) then
    !   do i = 1, ynum
    !        do j = 1, xnum
    !            write(221,'(1x,E,$)',iostat=statCode) temp(i,j)
    !            write(222,'(1x,E,$)',iostat=statCode) rho(i,j)
    !            write(223,'(1x,E,$)',iostat=statCode) melt_percentage(i,j)
    !        end do
    !    end do
        !$omp parallel
        !$omp sections
        !$omp section
    !    write(221,*,iostat=statCode) ' '
        !$omp section
    !    write(222,*,iostat=statCode) ' '
        !$omp section
    !    write(223,*,iostat=statCode) ' '
        !$omp end sections
        !$omp end parallel
    !end if write_file

end do time_loop

! close file and write data
if (comparison == 0) then
    !$omp parallel
    !$omp sections
    !$omp section
    close(221)
    !$omp section
    close(222)
    !$omp section
    close(223)
    !$omp end sections
    !$omp end parallel
    CALL write_data_file()
else
    CALL write_thickness_file()
end if



end program model

OBJECTS = model_mod.o model.o
.PHONY: clean help

model: $(OBJECTS)
	ifort $(OBJECTS)  -mkl -o $@

%.o: %.f90
	ifort $< -c -mkl

clean:
	rm *.o

help: 
	@echo "Valid targets: model_mod.f90 model.f90"

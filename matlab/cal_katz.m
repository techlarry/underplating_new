% This script is designed to calculate melt percentage(katz et al,2003)

% clearing all variables
clear;

% clear figures;
clf;

% basic constants
r0 = 0.5; % cpx/melt
r1 = 0.08; %cpx/melt/Gpa
beta1 = 1.50; % no unit
beta2 = 1.50; % no unit
K = 43; % C wt%^{-r}
gama = 0.75; % no unit
D_H20 = 0.01; % no unit
chi1 = 12.00; % wt%Gpa^{-lambda}
chi2 = 1.00; % wt%Gpa^{-1}
T0 = 273;
lambda = 0.6;



%% Figure 1. The anhydrous solidus, lherzolite liquidus and liquidus
P=0:0.1:8; % Gpa
T_solidus = 1085.7+132.9.*P-5.1.*P.^2 + T0; %K
T_liquidus_lherz = 1475.0+80.0.*P-3.2.*P.^2 + T0; %K
T_liquidus = 1780+45*P-2.*P.^2+T0; %K

figure(1);
clf;
hold on;
plot(T_solidus-T0,P,'k')
plot(T_liquidus_lherz-T0,P,'--')
plot(T_liquidus-T0,P,'*')
hold off;
box on;
xlabel('Temperature,\circ C')
ylabel('Pressure, GPa')
title('Anhydrous pseudo-phase diagram')
axis ij;
legend('Solidus','Liquidus lherz','Liquidus')
axis([1100 2100 0 8])

%% Figure 2. Isobaric anhydrous melting curves at different pressures with modal cpx of the unmelted rock at 15wt%
P = 3; % Gpa
T = 1000:1:2000; %C
T_solidus = 1085.7+132.9.*P-5.1.*P.^2; %C
T_liquidus_lherz = 1475.0+80.0.*P-3.2.*P.^2; %C
T_liquidus = 1780+45.*P-2.*P.^2; %C

F = zeros(size(T,2),1);
F_opx = zeros(size(T,2),1);
for i = 1:1:size(T,2)
    if T(i) > T_solidus
        F(i) = ((T(i)-T_solidus)./(T_liquidus_lherz-T_solidus)).^beta1;
    end
    R_cpx = r0 + r1.*P;
    F_cpx_out = 0.15./R_cpx;
    T_cpx_out = F_cpx_out.^(1/beta1).*(T_liquidus_lherz-T_solidus)+T_solidus;
    if F(i) > F_cpx_out
        F(i) = F_cpx_out+(1-F_cpx_out).*((T(i)-T_cpx_out)./(T_liquidus-T_cpx_out)).^beta2;
    end
end

figure(2);
clf;
hold on;
plot(T,F,'b')
axis([1000 2000 0 1])
box on;
grid on;


%% Figure 3. The solidus for different bulk water contents of the system. Solidus depression is linear with dissolved water. It is bounded by the saturation of water in the melt, a function of pressure.
X_H20_bulk = 0.05;
F = 0;
P = 0:0.1:8; % Gpa
X_H20 = X_H20_bulk/(D_H20+F*(1-D_H20)); 
X_sat = (chi1.*P.^(lambda)+chi2.*P);
delta_T = K*X_H20^(gama); % C
T_solidus = 1085.7+132.9.*P-5.1.*P.^2-delta_T; %C

T_sat = 1085.7+132.9.*P-5.1.*P.^2 - K.*(X_sat).^(gama);

 

figure(3);
clf;
hold on;
plot(T_solidus,P,'b')
plot(T_sat,P,'r')
hold off;
xlabel('Temperature,\circ C')
ylabel('Pressure, GPa')
title('hydrous pseudo-phase diagram')
axis ij;
axis([900 1900 0 8])

box on;


%% Figure 4. Isobaric melting curves (for 1 GPa) with different bulk water contents. The 0.3 wt% melting curve is saturated at the solidus.
% secant method

X_H20_bulk =0.3;
T = 900:1:1400; % C
P = 1; % Gpa
T_solidus = 1085.7+132.9.*P-5.1.*P.^2; %C
T_liquidus_lherz = 1475.0+80.0.*P-3.2.*P.^2; %C
T_liquidus = 1780+45.*P-2.*P.^2; %C
X_sat = (chi1.*P.^(lambda)+chi2.*P);

N0 = 1000; %
TOL = 0.001;
Fm = zeros(size(T,2),1);
Fm1 = zeros(size(T,2),1);

for k = 1:1:size(T,2)
    p = ((T(k)-T_solidus+K*(X_sat)^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    Fm1(k) = p;

%
    p0 = 0.01; % initial guess for F
    p1 = 0.3; % initial guess for F
    i = 2;
    q0 = p0 - ((T(k)-T_solidus+K*(X_H20_bulk/(D_H20+p0*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    q1 = p1 - ((T(k)-T_solidus+K*(X_H20_bulk/(D_H20+p1*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    while i <= N0
        p = p1-q1*(p1-p0)/(q1-q0);
        if p <= 0 %&&  imag(p)<0
            p = 0;
        end
        if abs(p-p1) < TOL
            break
        end
        i = i+1;
        p0 = p1;
        q0 = q1;
        p1 = p;
        q1 = p - ((T(k)-T_solidus+K*(X_H20_bulk/(D_H20+p*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    end
    if i < N0-2 %&& imag(p)>=0
        Fm(k) = p;
        fm_s = p;
    else
        Fm(k) = fm_s;
    end

            %}
%{
    if X_H20_bulk/(D_H20+p*(1-D_H20)) > X_sat
        sprintf('s')
        p = ((T(k)-T_solidus+K*(X_sat)^gama)/(T_liquidus_lherz-T_solidus))^beta1;
        if isreal(p)
            Fm(k) = p;
        end
    end
%}
end
figure(4);
clf;
hold on
plot(T, real(Fm1),'r');
scatter(T, real(Fm),'b');
xlabel('Temperature,\circ C')
ylabel('Pressure, GPa')
title('Isobaric(1Gpa) hydrous melting')
axis([900 1400 0 0.4])
box on;


%% Figure 4. Isobaric melting curves (for 1 GPa) with different bulk water contents. The 0.3 wt% melting curve is saturated at the solidus.
% test method
X_H20_bulk =0.3;
T = 900:1:1400; % C
P = 1; % Gpa
T_solidus = 1085.7+132.9.*P-5.1.*P.^2; %C
T_liquidus_lherz = 1475.0+80.0.*P-3.2.*P.^2; %C
T_liquidus = 1780+45.*P-2.*P.^2; %C
X_sat = (chi1.*P.^(lambda)+chi2.*P);

N0 = 1000; %
TOL = 0.001;
Fm = zeros(size(T,2),1);
Fm1 = zeros(size(T,2),1);
F = 0:0.001:0.2;
y = zeros(size(F,2),1);

for i = 1:1:size(F,2)
    
    
    y(i) =  F(i) - ((1100-T_solidus+K*(X_H20_bulk/(D_H20+F(i)*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;

end

figure(4);
clf;
hold on
scatter(F, real(y),'r');
xlabel('F')
ylabel('Function')
plot(F, zeros(size(F,2),1),'k')
box on;
grid on;

%% Figure 4. Isobaric melting curves (for 1 GPa) with different bulk water contents. The 0.3 wt% melting curve is saturated at the solidus.
X_H20_bulk =0.5;
T = 900:1:1500; % C
P = 1; % Gpa
T_solidus = 1085.7+132.9.*P-5.1.*P.^2; %C
T_liquidus_lherz = 1475.0+80.0.*P-3.2.*P.^2; %C
T_liquidus = 1780+45.*P-2.*P.^2; %C
X_sat = (chi1.*P.^(lambda)+chi2.*P);

N0 = 1000; % maximal number of iterations
TOL = 0.0001;
Fm = zeros(size(T,2),1);
Fm1 = zeros(size(T,2),1);

modal = 0.17;
R_cpx = r0 + r1.*P;
F_cpx_out = modal/R_cpx;
    
for k = 1:1:size(T,2)
    % determing  F when being saturate with water
    p = ((T(k)-T_solidus+K*(X_sat)^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    if imag(p) < 0
        p = 0;
    end
    Fm1(k) = p;
    

    % determing F when cpx
    p0 = 0.00; % initial guess for F
    p1 = 0.50; % initial guess for F
    i = 1;
    FA = p0 - ((T(k)-T_solidus+K*(X_H20_bulk/(D_H20+p0*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    while i <= N0
        p = (p0 + p1)/2;
        FP = p - ((T(k)-T_solidus+K*(X_H20_bulk/(D_H20+p*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
        if FP == 0 || abs(p0-p1) < TOL
            break
        end
        i = i+1;
        if FA*FP > 0
            p0 = p;
            FA = FP;
        else
            p1 = p;
        end
    end
    Fm(k) = p;
    
    
    % determing F when opx
    if p > F_cpx_out
        
        p0 = 0.00; % initial guess for F
        p1 = 0.80; % initial guess for F
        i = 1;
        delta_T = K*(X_H20_bulk/(D_H20+p0*(1-D_H20)))^gama;
        T_cpx_out = F_cpx_out.^(1/beta1).*(T_liquidus_lherz-T_solidus)+T_solidus;
        FA = p0 - F_cpx_out - (1-F_cpx_out).*((T(k)-T_cpx_out+delta_T)./(T_liquidus-T_cpx_out)).^beta2;
        while i <= N0
            p = (p0 + p1)/2;
            delta_T = K*(X_H20_bulk/(D_H20+p*(1-D_H20)))^gama;
            T_cpx_out = F_cpx_out.^(1/beta1).*(T_liquidus_lherz-T_solidus)+T_solidus;
            FP = p - F_cpx_out - (1-F_cpx_out).*((T(k)-T_cpx_out+delta_T)./(T_liquidus-T_cpx_out)).^beta2;
            if FP == 0 || abs(p0-p1) < TOL
                break
            end
            i = i+1;
            if FA*FP > 0
                p0 = p;
                FA = FP;
            else
                p1 = p;
            end
        end
        Fm(k) = p;
    end

end

figure(6);
clf;
hold on
plot(T, Fm1,'b--');
plot(T, Fm,'g--');
plot(T, min(Fm,Fm1),'r');

%plot(T, F_cpx_out*ones(size(T,2),1),'b');
hold off;
xlabel('Temperature,\circ C')
ylabel('Melt Fraction, F')
title('Isobaric(1Gpa) hydrous melting')
axis([900 1400 0 0.4])
box on;
grid on;


%% Figure 5a. Degree of melting as a function of the water in the system, holding the temperature  and pressure (1.5 GPa) constant.
X_H20_bulk =0:0.01:0.25;
T = 1200; % C
P = 1.5; % Gpa
T_solidus = 1085.7+132.9.*P-5.1.*P.^2; %C
T_liquidus_lherz = 1475.0+80.0.*P-3.2.*P.^2; %C
T_liquidus = 1780+45.*P-2.*P.^2; %C
X_sat = (chi1.*P.^(lambda)+chi2.*P);

N0 = 1000; % maximal number of iterations
TOL = 0.0001;
Fm = zeros(size(X_H20_bulk,2),1);
Fm1 = zeros(size(X_H20_bulk,2),1);
Fopx = zeros(size(X_H20_bulk,2),1);

modal = 0.17;
R_cpx = r0 + r1.*P;
F_cpx_out = modal/R_cpx;
T_cpx_out = F_cpx_out.^(1.0/beta1).*(T_liquidus_lherz-T_solidus)+T_solidus;
    
for k = 1:1:size(X_H20_bulk,2)
    % determing  F when being saturate with water
    p = ((T-T_solidus+K*(X_sat)^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    if imag(p) < 0
        p = 0;
    end
    Fm1(k) = p;
    

    % determing F when cpx
    p0 = 0.00; % initial guess for F
    p1 = 0.90; % initial guess for F
    i = 1;
    FA = p0 - ((T-T_solidus+K*(X_H20_bulk(k)/(D_H20+p0*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
    while i <= N0
        p = (p0 + p1)/2;
        FP = p - ((T-T_solidus+K*(X_H20_bulk(k)/(D_H20+p*(1-D_H20)))^gama)/(T_liquidus_lherz-T_solidus))^beta1;
        if FP == 0 || abs(p0-p1) < TOL
            break
        end
        i = i+1;
        if FA*FP > 0
            p0 = p;
            FA = FP;
        else
            p1 = p;
        end
    end
    Fm(k) = p;
    
    
    % determing F when opx
    if p > F_cpx_out
        
        p0 = 0.00; % initial guess for F
        p1 = 0.80; % initial guess for F
        i = 1;
        delta_T = K*(X_H20_bulk(k)/(D_H20+p0*(1-D_H20)))^gama;
        T_cpx_out = F_cpx_out.^(1/beta1).*(T_liquidus_lherz-T_solidus)+T_solidus;
        FA = p0 - F_cpx_out - (1-F_cpx_out).*((T-T_cpx_out+delta_T)./(T_liquidus-T_cpx_out)).^beta2;
        while i <= N0
            p = (p0 + p1)/2;
            delta_T = K*(X_H20_bulk(k)/(D_H20+p*(1-D_H20)))^gama;
            T_cpx_out = F_cpx_out.^(1/beta1).*(T_liquidus_lherz-T_solidus)+T_solidus;
            FP = p - F_cpx_out - (1-F_cpx_out).*((T-T_cpx_out+delta_T)./(T_liquidus-T_cpx_out)).^beta2;
            if FP == 0 || abs(p0-p1) < TOL
                break
            end
            i = i+1;
            if FA*FP > 0
                p0 = p;
                FA = FP;
            else
                p1 = p;
            end
        end
        Fm(k) = p;
    end

end

figure(7);
clf;
hold on
plot(X_H20_bulk, Fm,'b');
%plot(T, F_cpx_out*ones(size(T,2),1),'b');
xlabel('Bulk Water, wt%')
ylabel('Melt Fraction, F')
title('Isobaric(1.5Gpa) isothermal hydrous melting')
axis([0 0.25 0 0.25])
box on;
grid on;

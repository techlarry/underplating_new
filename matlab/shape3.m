% Author: larry
% Function shape2 define a shape like trapezoid. 

function [MI] = shape3(x, y, xnum, ynum, xsize, a1, a2, top, middle, uppercrust, moho_depth)

MI = int8(zeros(ynum, xnum)); 

for j = 1:1:xnum
    for i = 1:1:ynum
        
        if y(i) <= uppercrust  % upper crust
            MI(i,j) = 1;
        elseif y(i) <= moho_depth && y(i) > uppercrust % lower crust
            MI(i,j) = 2;
        else % mantle
            MI(i,j) = 3;            
        end
        
        % intrusive magma
        if  y(i)>= a2-a1 && y(i)<= a2...
                &&  y(i)>=-a1/(middle-top)*x(j)-1/2*(2*a1*middle-a1*top-a1*xsize-2*a2*middle+2*a2*top)/(middle-top)...
                &&  y(i)>=a1/(middle-top)*x(j)-1/2*(2*a1*middle-a1*top+a1*xsize-2*a2*middle+2*a2*top)/(middle-top)
            MI(i,j) = 4;    
        end
        
        
    end
end


end
% Thermal evolution 2D

%clearing all variables
clear;


%basic model parameters
Cp = 1.0e3; % specific heat [J/Kg/K]
d = 10e3; % characteristic length of radioactive heating [m]
La = 420e3; % latent heat of fusion  [J/kg]
Rgas = 8.3144598; % gas constant  [J/(mol*K)]


% thermal_expansion and thermal compressibility
thermal_expansion = 3e-5; % thermal expansion coefficient [k^-1]
thermal_compressibility = 1e-11; % thermal compressibility coefficient [pa^-1]

% bulk water fraction in weight percentage(0-0.5)
X_H20_bulk_intrusion = 0.00;   % water fraction for intrusion
X_H20_bulk_mantle = 0.00;      % water fraction for mantle

% intrusion shape control
top = 100e3; % size: the top of intrusion [m]:100-200e3
middle = 200e3; % size: the middle of intrusion [m]:200-400e3
bottom = 100e3; % size: the bottom of intrusion [m]:100-200e3

intrusion_thickness = 20e3; % the thickness of intrusion(m): 20-40e3
Tp = 1350; % the temperature of intrusive magma[C]: 1200-1650

% model size and step
xsize = 120e3; % the length of model horizontally [m]
xnum = 121;      % number of nodes along x
dx = xsize/(xnum-1); % grid step along x
x = linspace(0, xsize, xnum); % x coordinate


%% Calculating Liquidus and Solidus based on  katz, 2003
% 0. solidus, liquidus and melt  constants(from katz, 2003)
K = 43; % C wt%^{-r}
gama = 0.75; % no unit
D_H20 = 0.01; % no unit
chi1 = 12.00; % wt%Gpa^{-lambda}
chi2 = 1.00; % wt%Gpa^{-1}
lambda = 0.6;
modal = 0.17;
N0 = 1000; % maximal number of iterations
TOL = 0.001; % tolerance

Ts = zeros(xnum, 1); % solidus temperature;
Tl = zeros(xnum, 1); % liquidus temperature;

for j = 1:1:xnum
        
    h = (i-1)*dx; % depth [m]
    P = h/28e3*1e9;  %pressure [Pa]
    PG = h/28e3;  %pressure [GPa]

    % 1. Solidus Temperature
    F = 0; % no melt
    X_H20 = X_H20_bulk_intrusion/(D_H20+F*(1-D_H20));  % dissovled water fraction in the melt
    X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
    delta_T = K*X_H20^(gama); % C
    T_solidus = 1085.7+132.9.*PG-5.1.*PG.^2-delta_T; %C
    T_sat = 1085.7+132.9.*PG-5.1.*PG.^2 - K.*(X_sat).^(gama);%C
    if X_H20 < X_sat
        Ts(j) = T_solidus + T0; %K
    else
        Ts(j) = T_sat + T0; %K
    end

    % 2. liquidus Temperature
    F = 1; % complete melt
    X_H20 = X_H20_bulk_intrusion/(D_H20+F*(1-D_H20));  % dissovled water fraction in the melt
    X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
    delta_T = K*X_H20^(gama); % C
    T_liquidus = 1780+45.*PG-2.*PG.^2-delta_T; %C
    T_liquidus_lherz = 1475.0+80.0.*PG-3.2.*PG.^2-delta_T; %C
    T_sat = 1085.7+132.9.*PG-5.1.*PG.^2 - K.*(X_sat).^(gama);%C
    if X_H20 < X_sat
        Tl(j) = T_liquidus + T0; %K
        Tll(j) = T_liquidus_lherz + T0;
    else
        Tl(j) = T_sat + T0; %K
        Tll(j) = T_sat + T0; %K
    end

end



%% Mantle upwelling: P-T-X Calculation (Mckenzie,1984)
% basic paramenter
Cp_Dan = 1.2e3; % heat capacity: J/kg/K
alpha_s_rho_s= 1.2e-8; % the ratio of alpha_s and rho_s: m^3/kg/K
alpha_m_rho_m= 2.4e-8; % the ratio of alpha_m and rho_m: m^3/kg/K
delta_s = 362; % entropy increment: J/kg/K
rho_f = 2500; % melt magma density:kg/m^3
rho_s = 3300; % mantle density:kg/m^3


% function dx/dp
f1 = @(P,X) (-Cp_Dan/(1100+273+100/1e9+600*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1100+273+100/1e9+600*X)*600); % a simple form for DX/DP
f2 = @(P,X) (-Cp_Dan/(1115+273+120/1e9*P+(600-136/1e9*P)*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1115+273+120/1e9*P+(600-136/1e9*P)*X)*600); % second form when P < 3.5Gpa
f3 = @(P,X) (-Cp_Dan/(1115+imLarry!
273+120/1e9*P+124*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1115+273+120/1e9*P+124*X)*600); % second form when P > 3.5Gpa

% function T(P,X)
f4 = @(P,X) 1115+273+120/1e9*P+(600-136/1e9.*P).*X; %  when P < 3.5Gpa
f5 = @(P,X) 1115+273+120/1e9*P+124*X; %  when P > 3.5Gpa

% set solids 
P_solidus = Ps_Lee;  % solidus temperature: K
%T_solidus = Ts_Lee;  % solidus pressure: Pa
T_solidus = f5(P_solidus, 0);
Ds = Ds_Lee; % solidus depth:m
Expression = -1; % when expression large than 0, f1 will be used; otherwise, f2 and f3;


figure(110);
clf; % clear the figure
clr = lines(500);    % some colormap
ax1 = subplot(1,2,1); % first figure
ax2 = subplot(1,2,2); % second figure
axis(ax1,[1100 1800 0 xsize/1e3])
axis(ax1, 'ij')
box(ax1, 'on')
hold(ax1);
xlabel(ax1,'Temperature(C)','FontSize', 14)
ylabel(ax1,'Depth(km)','FontSize', 14)
title(ax1,'Isentropic Mantle Upwelling','FontSize', 16)
set(ax1,'xaxislocation','top')


axis(ax2,[0 50 0 xsize/1e3])% plot total volume of melt present below a given depth
hold(ax2);
box(ax2, 'on')
xlabel(ax2,'Melt thickness(km)','FontSize', 14)
ylabel(ax2,'Depth(km)','FontSize', 14)
title(ax2,'Total volume of melt percent','FontSize', 16)
axis(ax2, 'ij')
set(ax2,'xaxislocation','top')


% plot solidus
Plot_1(1) = plot(ax1, T_solidus-T0,Ds/1e3, '--');
Leg1{1} = 'Solidus'; % set legend cell

% secondly, integrating DX/DP by runge-kutta methiod(forth order)
N = 30; % the number of points to calculate each melting path
phi = zeros(N+1,xnum);  % volume fraction of melt
thickness_melt = zeros(N+1,xnum); % the integration of melt thickness when mantle upwelling from solidus: m
depth = zeros(N+1,xnum); % depth: Km
kk = 1; % count plot number
max_p = 6e9; % max pressure for plot: Pa
for j = 1:1:xnum % different j means different upper_pressure on solidus
    a = P_solidus(j);
    b = 0;
    h = (b-a)/N; % pressure interval: Pa
    p = a;
    w = 0; % intial X(P,T); w sets to 0, which means the melting path begin from solidus
    dz = abs(h)/1e9*28e3; % depth interval: m
    
    depth(1,j) = Ds(1); % depth: m
    X = zeros(N+1, 1); % weight melting percentage
    X(1) = 0;  % set 0 because of soliuds
    P = zeros(N+1, 1); % Pressure: Pa
    P(1) = p; % set p because of upper bound pressure
    
    for i=1:1:N
        
        if Expression > 0
            f = f1; % using expression f1
        elseif p < 3.5e9
            f = f2; % using expression f2
        elseif p > 3.5e9 || p == 3.5e9
            f = f3; % using expression f3
        end
        
        k1 = h*f(p,w);
        k2 = h*f(p+h/2, w+k1/2);
        k3 = h*f(p+h/2, w+k2/2);
        k4 = h*f(p+h, w+k3);
        
        w =  w + (k1+2*k2+2*k3+k4)/6;
        p =  P_solidus(j) + i*h;  % decrease pressure by pressure interval
        X(i+1) = w; % store the current pelt percent to X
        P(i+1) = p; % store the current pressure to P
        
        phi(i+1,j) = w*rho_s/(rho_f+w*(rho_s-rho_f));  % volume fraction of melt
        thickness_melt(i+1,j) = thickness_melt(i,j) + phi(i+1,j)*dz; % the integration of melt thickness when mantle upwelling from solidus: m
        depth(i+1,j) = p/1e9*28e3; % store the current depth to depth: m
    end
    if p < 3.5e9
        f = f4; % using expression f4
    elseif p > 3.5e9 || p == 3.5e9
        f = f5; % using expression f5
    end
    T = f(P, X); % temperature: K
    temp_reso = 3; % temperature resoluation
    if  max_p > P_solidus(j)  % choose only large part
        if j/temp_reso == round(j/temp_reso) && max_p > P_solidus(j) 
            i = 1;
            p_low = zeros(round((max_p - P_solidus(j))/abs(h)),1);
            T_low = zeros(round((max_p - P_solidus(j))/abs(h)),1);
            p_low(1) = P_solidus(j); % pressure below solidus
            T_low(1) = T_solidus(j); % temperature below solidus
            while p_low(i) < max_p
                T_low(i+1) = T_low(i) + Grad_ad*dz;  % temperautre below solidus
                p_low(i+1) =  p_low(i) + abs(h);  % increase pressure by pressure interval
                i = i+1;
            end
            if T_low(i) < 1510 +T0 && T_low(i) > 1490 +T0
                plot(ax1, T-273, P/1e9*28,'Color',clr(j/temp_reso,:)) % plot current melting path
                plot(ax2, thickness_melt(2:end,j)/1e3, depth(2:end,j)/1e3,'Color',clr(j/temp_reso,:)); % plot thickness
                Plot_1(kk+1) = plot(ax1, T_low-273, p_low/1e9*28,'Color',clr(j/temp_reso,:));
                Leg1{kk+1} = [num2str(round((T_low(i)-T0)/temp_reso)*temp_reso),'\circC'];
                kk = kk+1;
            end
        end
    end
end
ax1_leg = legend(Plot_1,Leg1);
set(ax1_leg,'FontSize',12);





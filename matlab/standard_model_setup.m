% Author: wang zhenhua

% This simple script establish a standard intrusion data set, which can be used in determining 
% thermal evolution, because all changed parameters in other models should be compared with 
% the standard model.


%% standard model info
X_H20_bulk_intrusion = 0.00;   % water fraction for intrusion
X_H20_bulk_mantle = 0.00;      % water fraction for mantle

% intrusion shape control
shape = 1; % shape, choose 1-3
top = 100e3; % size: the top of intrusion [m]:100-200e3
middle = 200e3; % size: the middle of intrusion [m]:200-400e3
bottom = 100e3; % size: the bottom of intrusion [m]:100-200e3
intrusion_thickness = 20e3; % the thickness of intrusion(m): 20-40e3

% intrusion temperature
Tp = 1300; % the temperature of intrusive magma[C]: 1200-1650
% This script produces a comparison data set, called Namelist.nml, which can be used in fortran
% programs in order to compare the thickness of underplating with different
% water percentage, underplating temperature and the thickness of
% intrusion. In addition, it gives the underplating thinkness directly according to the
% matlab function model7_comparison which set the variable tnum to 1, thus
% can be pictured immediately.


% clearing all data
clear;
% clearing command window
clc;


%% model comparison set parameters
model_comparison_parameter_set

%% set three kinds of underplating thickness
u1 = 5e3;   % underplating thickness [m]
u2 = 10e3;   % underplating thickness [m]
%% open file
fid = fopen('Namelist.nml','w');
fid2 = fopen('compariosn_thickness.dat','w');

%% matrix initialize
underplating_thickness_vs = zeros(size(wp,2),size(ut,2));
underplating_melt_percentage_vs = zeros(size(wp,2),size(ut,2));
save_granitic_thickness_vs = zeros(size(wp,2),size(ut,2));
underplating_thickness_kind = zeros(size(wp,2),size(ut,2));

%% water v.s. temp

% initialize plot
figure(550)
clf;
hold on;
xlabel('Water(ppt)')
ylabel('Temp(^\circC)')
axis([min(wp) max(wp) min(ut) max(ut)])


% standard model setup
standard_model_setup
count = 0;

% water and temp cycle
i_dummy = 1; % distinguish i
for water = wp
    
    j_dummy = 1; % distinguish j
    for Tp = ut
        
        
        %set parameter in Model7_comparison
        X_H20_bulk_intrusion = water/1000;  % water fraction for intrusion
        X_H20_bulk_mantle = water/1000;     % water fraction for mantle
        
        % run Model7_comparison
        Model7_comparison
        
        % save data
        underplating_thickness_vs(i_dummy, j_dummy) = underplating_thickness;
        underplating_melt_percentage_vs(i_dummy, j_dummy) = underplating_melt_percentage;
        save_granitic_thickness_vs(i_dummy, j_dummy) = save_granitic_thickness;
        

        % data
        count = count +1;
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'shape=', num2str(shape),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 
        comparison_thickness = [num2str(count), ' ', num2str(underplating_thickness)];
        fprintf(fid2,'%s\n',comparison_thickness); 
    
        
        % differentiate
        if underplating_thickness <= u1
            
            underplating_thickness_kind(i_dummy, j_dummy) = 1;
            plot(wp(i_dummy), ut(j_dummy),'^b')
            
        elseif underplating_thickness <= u2
            
            underplating_thickness_kind(i_dummy, j_dummy) = 2;
            plot(wp(i_dummy), ut(j_dummy),'or')
            
        else
            
            underplating_thickness_kind(i_dummy, j_dummy) = 3;
            plot(wp(i_dummy), ut(j_dummy),'xg')
            
        end
        
        
        % print 
        sprintf('i=%d,j=%d',i_dummy,j_dummy)
        
        j_dummy = j_dummy +1;
    end
    i_dummy = i_dummy + 1;
end
title('Water v.s. Temp')

%% temp v.s. intrusion thickness
% initialize plot
figure(551)
clf;
hold on;
xlabel('Temp(^\circC)')
ylabel('Intrusion thickness(Km)')
axis([min(ut) max(ut) min(ui)/1e3 max(ui)/1e3])

% standard model setup
standard_model_setup

% temp and intrusion thickness cycle
i_dummy = 1; % distinguish i
for Tp = ut
    
    j_dummy = 1; % distinguish j
    for intrusion_thickness = ui
        
        
        % run Model7_comparison
        Model7_comparison
        
        % save data
        underplating_thickness_vs(i_dummy, j_dummy) = underplating_thickness;
        underplating_melt_percentage_vs(i_dummy, j_dummy) = underplating_melt_percentage;
        save_granitic_thickness_vs(i_dummy, j_dummy) = save_granitic_thickness;
        
        
        % data
        count = count +1;
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'shape=', num2str(shape),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 
        comparison_thickness = [num2str(count), ' ', num2str(underplating_thickness)];
        fprintf(fid2,'%s\n',comparison_thickness); 
        
        
        % differentiate
        if underplating_thickness <= u1
            
            underplating_thickness_kind(i_dummy, j_dummy) = 1;
            plot(ut(i_dummy), ui(j_dummy)/1000,'^b')
            
        elseif underplating_thickness <= u2
            
            underplating_thickness_kind(i_dummy, j_dummy) = 2;
            plot(ut(i_dummy), ui(j_dummy)/1000,'or')
            
        else
            
            underplating_thickness_kind(i_dummy, j_dummy) = 3;
            plot(ut(i_dummy), ui(j_dummy)/1000,'xg')
            
        end
        
        
        % print 
        sprintf('i=%d,j=%d',i_dummy,j_dummy)
        
        j_dummy = j_dummy +1;
    end
    i_dummy = i_dummy + 1;
end
title('Temp v.s. Intrusion thicnkess')

%% water v.s. intrusion thickness
% initialize plot
figure(552)
clf;
hold on;
xlabel('Water(ppt)')
ylabel('Intrusion thickness(Km)')
axis([min(wp) max(wp) min(ui)/1e3 max(ui)/1e3])

% standard model setup
standard_model_setup

% temp and intrusion thickness cycle
i_dummy = 1; % distinguish i
for water = wp
    
    j_dummy = 1; % distinguish j
    for intrusion_thickness = ui
       
        
        % set parameter in Model7_comparison
        X_H20_bulk_intrusion = water/1000;  % water fraction for intrusion
        X_H20_bulk_mantle = water/1000;     % water fraction for mantle
        
        % run Model7_comparison
        Model7_comparison
        
        % save data
        underplating_thickness_vs(i_dummy, j_dummy) = underplating_thickness;
        underplating_melt_percentage_vs(i_dummy, j_dummy) = underplating_melt_percentage;
        save_granitic_thickness_vs(i_dummy, j_dummy) = save_granitic_thickness;
        
        
        % data
        count = count +1;
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'shape=', num2str(shape),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 
        comparison_thickness = [num2str(count), ' ', num2str(underplating_thickness)];
        fprintf(fid2,'%s\n',comparison_thickness); 
        
        
        % differentiate
        if underplating_thickness <= u1
            
            underplating_thickness_kind(i_dummy, j_dummy) = 1;
            plot(wp(i_dummy), ui(j_dummy)/1000,'^b')
            
        elseif underplating_thickness <= u2
            
            underplating_thickness_kind(i_dummy, j_dummy) = 2;
            plot(wp(i_dummy), ui(j_dummy)/1000,'or')
            
        else
            
            underplating_thickness_kind(i_dummy, j_dummy) = 3;
            plot(wp(i_dummy), ui(j_dummy)/1000,'xg')
            
        end
        
        
        % print 
        sprintf('i=%d,j=%d',i_dummy,j_dummy)
        
        j_dummy = j_dummy +1;
    end
    i_dummy = i_dummy + 1;
end
title('Water v.s. Intrusion thicnkess')
 


% Author: wang zhenhua 

% This script produces a comparison data set, called Namelist.nml, which can be used in fortran
% programs in order to compare the thickness of underplating with different
% water percentage, underplating temperature and the thickness of
% intrusion.


%% clearing all data
clear;
% clearing command window
clc;

%% model comparison set parameters
model_comparison_parameter_set


%% set three kinds of underplating thickness
u1 = 5e3;   % underplating thickness [m]
u2 = 10e3;   % underplating thickness [m]


%% open file
fid = fopen('Namelist.nml','w');

%% water v.s. temp

% water and temp cycle
standard_model_setup
count = 0;
i_dummy = 1; % distinguish i
for water = wp
    
    j_dummy = 1; % distinguish j
    for Tp = ut
        
        count = count +1;
        % set parameter in Model7_comparison
        X_H20_bulk_intrusion = water/1000;  % water fraction for intrusion
        X_H20_bulk_mantle = water/1000;     % water fraction for mantle
        
        % data
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 
        
        j_dummy = j_dummy +1;
    end
    i_dummy = i_dummy + 1;
end

%% temp v.s. intrusion thickness


% temp and intrusion thickness cycle
standard_model_setup
i_dummy = 1; % distinguish i
for Tp = ut
    
    j_dummy = 1; % istinguish j
    for intrusion_thickness = ui
        
        count = count +1;

        
        % data
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 

        
        j_dummy = j_dummy +1;
    end
    i_dummy = i_dummy + 1;
end

%% water v.s. intrusion thickness

% temp and intrusion thickness cycle
standard_model_setup
i_dummy = 1; % distinguish i
for water = wp
    
    j_dummy = 1; % distinguish j
    for intrusion_thickness = ui
        
        count = count +1;
        % set parameter in Model7_comparison
        X_H20_bulk_intrusion = water/1000;  % water fraction for intrusion
        X_H20_bulk_mantle = water/1000;     % water fraction for mantle
        
        
        % data
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 
        
        j_dummy = j_dummy +1;                                                                                                                      
    end
    i_dummy = i_dummy + 1;
end 

% close file
fclose(fid);
%% P-T-X calculation
% by Dan Mckenzie, 1984

% clearing all variables
clear;

% basic paramenter, from Iwamori et al.1995
Cp_Dan = 1.2e3; % heat capacity: J/kg/K
alpha_s_rho_s= 1.2e-8; % the ratio of alpha_s and rho_s: m^3/kg/K
alpha_m_rho_m= 2.4e-8; % the ratio of alpha_m and rho_m: m^3/kg/K
delta_s = 350; % entropy increment: J/kg/K
rho_f = 2500; % melt magma density:kg/m^3
rho_s = 3300; % mantle density:kg/m^3


% function dx/dp
f1 = @(P,X) (-Cp_Dan/(1100+273+100/1e9+600*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1100+273+100/1e9+600*X)*600); % a simple form for DX/DP
f2 = @(P,X) (-Cp_Dan/(1115+273+120/1e9*P+(600-136/1e9*P)*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1115+273+120/1e9*P+(600-136/1e9*P)*X)*600); % second form when P < 3.5Gpa
f3 = @(P,X) (-Cp_Dan/(1115+273+120/1e9*P+124*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1115+273+120/1e9*P+124*X)*600); % second form when P > 3.5Gpa
Expression = -1; % when expression large than 0, f1 will be used; otherwise, f2 and f3;



%%  solidus

% define P
PG = 0:0.1:8; % PG means Pressure [GPa] 

% Solidus Temperature
X_H20_bulk_mantle = 0.00;
D_H20 = 0.01;
F = 0; % no melt
chi1 = 12.00 ;
chi2 = 1.00;
lambda = 0.6;
gama = 0.75;
KI = 43;
X_H20 = X_H20_bulk_mantle/(D_H20+F*(1-D_H20)); % dissovled water fraction in the melt
X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
delta_T = KI*X_H20.^(gama); % C
T_solidus = 1085.7+132.9.*PG-5.1.*PG.^2-delta_T; %C
T_sat = 1085.7+132.9*PG-5.1*PG.^2 - KI*(X_sat).^(gama); %C
Ts = T_solidus;

% plotting figure
figure(1);
clf; % clearing the figure content
%axis([1100 1800 0 5*28])  % 5*28 means 5Gpa*28km/Gpa
box on;
hold on;
xlabel('Temperature(^\circ C)')
ylabel('Depth(km)')
axis ij
set(gca,'xaxislocation','top')

plot(Ts, PG.*28,'--')



%% secondly, integrating DX/DP by runge-kutta methiod(forth order)
upper_pressure = 8e9;  % upper bound pressure of simulation: Pa
lower_pressure = 0e9; % lower bound pressure of simulation: Pa
N = 20; % the number of points to calculate each melting path
pressure_decrease_interval = 0.5*1e9; % decrease upper pressure 0.5Gpa each time
iter_num = (upper_pressure-lower_pressure)/pressure_decrease_interval+1;
phi = zeros(N+1,iter_num);  % volume fraction of melt
thickness_melt = zeros(N+1,iter_num); % the integration of melt thickness when mantle upwelling from solidus: m
depth = zeros(21,10); % depth: Km
for j = 1:1:iter_num  % different j means different upper_pressure on solidus
    h = (lower_pressure - upper_pressure)/N; % pressure interval: Pa
    p = upper_pressure;  % pressure at depth(1,j): Pa
    depth(1,j) = p/1e9*28; % depth: Km
    w = 0; % intial X(P,T); w sets to 0, which means the melting path begin from solidus
    dz = abs(h)/1e9*28; % depth interval: m
    
    
    X = zeros(N+1, 1); % weight melting percentage
    X(1) = 0;  % set 0 because of soliuds
    P = zeros(N+1, 1); % Pressure: Pa
    P(1) = p; % set p because of upper bound pressure
    
    for i=1:1:N
        
        if Expression > 0
            f = f1; % using expression f1
        elseif p < 3.5e9
            f = f2; % using expression f2
        elseif p > 3.5e9 || p == 3.5e9
            f = f3; % using expression f3
        end
        
        k1 = h*f(p,w);
        k2 = h*f(p+h/2, w+k1/2);
        k3 = h*f(p+h/2, w+k2/2);
        k4 = h*f(p+h, w+k3);
        
        w =  w + (k1+2*k2+2*k3+k4)/6;
        p = upper_pressure + i*h;  % increase pressure by pressure interval
        X(i+1) = w; % store the current pelt percent to X
        P(i+1) = p; % store the current pressure to P
        
        phi(i+1,j) = w*rho_s/(rho_f+w*(rho_s-rho_f));  % volume fraction of melt
        thickness_melt(i+1,j) = thickness_melt(i,j) + phi(i+1,j)*dz; % the integration of melt thickness when mantle upwelling from solidus: m
        depth(i+1,j) = p/1e9*28; % % store the current depth to depth: Km
    end
    T = 1100+273+100/1e9*P+600*X; % temperature: K
    plot(T-273,P/1e9*28) % plot current melting path
    upper_pressure = upper_pressure - pressure_decrease_interval; % decrease upper pressure 0.5Gpa each time
    
end


%% plot total volume of melt present below a given depth
figure(3)
plot(thickness_melt, depth)
ylabel('Depth(km)')
xlabel('Melt thickness(km)')
title('Total volume of melt percent')
legend('1650','1600','1550','1500','1450','1400','1350','1300','1250','1200')
axis ij
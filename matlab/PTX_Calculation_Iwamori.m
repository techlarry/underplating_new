%% P-T-X calculation
% by Dan Mckenzie, 1984

% clearing all variables
clear;

% basic paramenter, from Iwamori et al.1995
Cp_Dan = 1.2e3; % heat capacity: J/kg/K
alpha_s_rho_s= 1.2e-8; % the ratio of alpha_s and rho_s: m^3/kg/K
alpha_m_rho_m= 2.4e-8; % the ratio of alpha_m and rho_m: m^3/kg/K
delta_s = 350; % entropy increment: J/kg/K
rho_f = 2500; % melt magma density:kg/m^3
rho_s = 3300; % mantle density:kg/m^3


% function dx/dp
f1 = @(P,X) (-Cp_Dan/(1100+273+100/1e9+600*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1100+273+100/1e9+600*X)*600); % a simple form for DX/DP
f2 = @(P,X) (-Cp_Dan/(1115+273+120/1e9*P+(600-136/1e9*P)*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1115+273+120/1e9*P+(600-136/1e9*P)*X)*600); % second form when P < 3.5Gpa
f3 = @(P,X) (-Cp_Dan/(1115+273+120/1e9*P+124*X)*100/1E9+alpha_s_rho_s+(alpha_m_rho_m-alpha_s_rho_s)*X)/(delta_s+Cp_Dan/(1115+273+120/1e9*P+124*X)*600); % second form when P > 3.5Gpa
Expression = -1; % when expression large than 0, f1 will be used; otherwise, f2 and f3;


%% firstly, plotting figure
figure(2);
clf; % clearing the figure content
axis([1100 1600 0 4])
box on;
hold on;
xlabel('Temperature(C)')
ylabel('Pressure(Gpa)')
axis ij
set(gca,'xaxislocation','top')

% plot solidus
P_solidus = 0e9:0.1e9:5e9;
size_solidus = size(P_solidus);
T_solidus = zeros(size_solidus(2),1);
for i = 1:1:size_solidus(2)
    T_solidus(i) = 1100+273+100/1e9*P_solidus(i);
end
plot(T_solidus-273,P_solidus/1e9,'--')


%% secondly, integrating DX/DP by runge-kutta methiod(forth order)
upper_pressure = 5e9;  % upper bound pressure of simulation: Pa
lower_pressure = 0e9; % lower bound pressure of simulation: Pa
N = 20; % the number of points to calculate each melting path
pressure_decrease_interval = 0.5*1e9; % decrease upper pressure 0.5Gpa each time
iter_num = (upper_pressure-lower_pressure)/pressure_decrease_interval+1;
phi = zeros(N+1,iter_num);  % volume fraction of melt
thickness_melt = zeros(N+1,iter_num); % the integration of melt thickness when mantle upwelling from solidus: m
depth = zeros(21,10); % depth: Km
for j = 1:1:iter_num  % different j means different upper_pressure on solidus
    h = (lower_pressure - upper_pressure)/N; % pressure interval: Pa
    p = upper_pressure;  % pressure at depth(1,j): Pa
    depth(1,j) = p/1e9*28; % depth: Km
    w = 0; % intial X(P,T); w sets to 0, which means the melting path begin from solidus
    dz = abs(h)/1e9*28; % depth interval: m
    
    
    X = zeros(N+1, 1); % weight melting percentage
    X(1) = 0;  % set 0 because of soliuds
    P = zeros(N+1, 1); % Pressure: Pa
    P(1) = p; % set p because of upper bound pressure
    
    for i=1:1:N
        
        if Expression > 0
            f = f1; % using expression f1
        elseif p < 3.5e9
            f = f2; % using expression f2
        elseif p > 3.5e9 || p == 3.5e9
            f = f3; % using expression f3
        end
        
        k1 = h*f(p,w);
        k2 = h*f(p+h/2, w+k1/2);
        k3 = h*f(p+h/2, w+k2/2);
        k4 = h*f(p+h, w+k3);
        
        w =  w + (k1+2*k2+2*k3+k4)/6;
        p = upper_pressure + i*h;  % increase pressure by pressure interval
        X(i+1) = w; % store the current pelt percent to X
        P(i+1) = p; % store the current pressure to P
        
        phi(i+1,j) = w*rho_s/(rho_f+w*(rho_s-rho_f));  % volume fraction of melt
        thickness_melt(i+1,j) = thickness_melt(i,j) + phi(i+1,j)*dz; % the integration of melt thickness when mantle upwelling from solidus: m
        depth(i+1,j) = p/1e9*28; % % store the current depth to depth: Km
    end
    T = 1100+273+100/1e9*P+600*X; % temperature: K
    plot(T-273,P/1e9) % plot current melting path
    upper_pressure = upper_pressure - pressure_decrease_interval; % decrease upper pressure 0.5Gpa each time
    
end


%% plot total volume of melt present below a given depth
figure(3)
plot(thickness_melt, depth)
ylabel('Depth(km)')
xlabel('Melt thickness(km)')
title('Total volume of melt percent')
legend('1650','1600','1550','1500','1450','1400','1350','1300','1250','1200')
axis ij
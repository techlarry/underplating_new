% clear
clear;
clc;
T0 = 273.15; % paramter: K

underplating_thickness = zeros(20,1); % underlating thickness: m
granitic_thickness = zeros(20,1); % grantitic thickness: m

for i = 1:1:20
    
    Model = ['MODEL',int2str(i),'_'];

    % read xy data
    x = importdata([Model, 'x_coordinate.dat']);
    y = importdata([Model, 'y_coordinate.dat']);

    %size
    xnum = size(x,2);
    ynum = size(y,2);
    xsize = x(xnum);
    ysize = y(ynum);
    
    %%   save_underplating_thickness
    save_underplating_thickness = importdata([Model,'save_underplating_thickness.dat']);
    underplating_thickness(i) = save_underplating_thickness(1);
    
        
    %%   save_underplating_thickness
    save_granitic_thickness = importdata([Model,'save_granitic_thickness.dat']);
    granitic_thickness(i) = max(save_granitic_thickness);
  
end

%% save reulst
model_result = [underplating_thickness,granitic_thickness];
save('MODEL_RESULT.mat','model_result')

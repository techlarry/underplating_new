%%%%%
%author: wangzhenhua
%title: thermal evolution

% The script MODEL7 calculate thermal evolutions of specific
% underplating models based on different intrusion shapes, intrusion
% temperatures and water percentages. The temperature of solidus and liquidus
% used in the script is calculated according to katz et al, 2003. The final
% result is showed in the form of figure and data both of which will be
% stored in order to examine carefully. The stored data contains surface
% heat flux and its rate, lithospheric strength, intrusion thinkness and
% melt percentages in both intrusion and grantic rock.



%clearing all variables
clear;


%basic model parameters
Cp = 1.0e3; % specific heat [J/Kg/K]
d = 10e3; % characteristic length of radioactive heating [m]
La = 420e3; % latent heat of fusion  [J/kg]
Rgas = 8.3144598; % gas constant  [J/(mol*K)]


% thermal_expansion and thermal compressibility
thermal_expansion = 3e-5; % thermal expansion coefficient [k^-1]
thermal_compressibility = 1e-11; % thermal compressibility coefficient [pa^-1]

% model size and step
xsize = 601e3; % the length of model horizontally [m]
ysize = 111e3; % the length of model vertically [m]
xnum = 101;      % number of nodes along x
ynum = 101;       % number of nodes along y
dx = xsize/(xnum-1); % grid step along y
dy = ysize/(ynum-1); % grid step along x
x = linspace(0, xsize, xnum); % y coordinate
y = linspace(0, ysize, ynum); % x coordinate

% define time step
tnum = 1000;  % number of time step
kappa_dt = 1.0e-6; % just for define time step
dt = min(dx,dy)^2/3/kappa_dt*0.75; % time step[s]
dtmy = dt/(1e6*365.25*24*3600); % time step [a million year]



% solidus, liquidus and melt  constants(from katz, 2003)
K = 43; % C wt%^{-r}
gama = 0.75; % no unit
D_H20 = 0.01; % no unit
chi1 = 12.00; % wt%Gpa^{-lambda}
chi2 = 1.00; % wt%Gpa^{-1}
lambda = 0.6;
modal = 0.17;
N0 = 1000; % maximal number of iterations
TOL = 0.001; % tolerance



%% MODEL info

% Depths
uppercrust = 14e3; % the depth of uppercrust:m
moho_depth = 40e3; % the depth of moho:m

% Temp
T0 = 273.15; % paramter: K
Tsur = 0; % surface temperature:C

% sendiment and water density
rhos = 2300; % sendiment denisty: kg/m^3
rhow = 1030; % water denisty: kg/m^3
rhol = 2800; % lithoshere denisty: kg/m^3
rhom = 3300; % mantle denisty: kg/m^3

% radiogenic heat control: if Aro_exp = 0, the radiogenic heat stay the same with time
Aro_exp = -3.0*1e-4; 

% strain rate
sr = 1e-15; 


% LAB tempearture: C
Tm = 1350; %[C]

%%  Variables for parameter test: all parameters can change 

% model number
model_number = 7;

% bulk water fraction in weight percentage(0-0.5)
X_H20_bulk_intrusion = 0.00;   % water fraction for intrusion
X_H20_bulk_mantle = 0.00;      % water fraction for mantle

% intrusion shape control
top = 100e3; % size: the top of intrusion [m]:100-200e3
middle = 200e3; % size: the middle of intrusion [m]:200-400e3
bottom = 100e3; % size: the bottom of intrusion [m]:100-200e3

intrusion_thickness = 20e3; % the thickness of intrusion(m): 20-40e3
Tp = 1350; % the temperature of intrusive magma[C]: 1200-1650

shape = 2; % shape, choose 1-3


%% Define SHAPE
% type of rock: 1--upper crust, 2--lower crust, 3--mantle, 4--intrusive mantle, 5--melted crust, 6--melted mantle, 7--melted intrusive mantle

switch shape
    
    case 1
        
        MI = shape1(x, y, xnum, ynum, xsize, intrusion_thickness, moho_depth+intrusion_thickness/2, bottom, top, middle, uppercrust, moho_depth);
    
    case 2
        
        MI = shape2(x, y, xnum, ynum, xsize, intrusion_thickness, moho_depth+intrusion_thickness, bottom, middle, uppercrust, moho_depth);
    
    case 3
        
        MI = shape3(x, y, xnum, ynum, xsize, intrusion_thickness, moho_depth, top, middle, uppercrust, moho_depth);
    
    otherwise
      
        print('shape  Wrong');
        
end
%
%{ debug
figure(2);
clf;
hold on;
imagesc(x/1e3, y/1e3, MI)
axis ij image
axis([0 xsize/1e3 0 ysize/1e3])
xlabel('Distance(km)','FontSize', 14)
ylabel('Depth(km)','FontSize', 14)
title('Material type Distribution')
box on;
shading interp;
h = colorbar;
h.Label.String = 'MI';
%}



%% no intrusive property: define temperature and density before instrusion

% initialize
no_intrusive_temp = (ones(xnum,1)*(y/ysize*Tm + T0))'; % temperature:K
no_intrusive_rho = zeros(ynum, xnum); % density distribution when no intrusive


% node cycle
for j = 1:1:xnum
    for i = 1:1:ynum
        
        h = (i-1)*dy; % depth: m
        P = h/28e3*1e9;  %pressure: Pa
        if y(i) <= uppercrust  % upper crust
            no_intrusive_rho(i,j) = 2800*(1-thermal_expansion*(no_intrusive_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % crust density:kg/m^3
        elseif y(i) <= moho_depth && y(i) > uppercrust % lower crust
            no_intrusive_rho(i,j) = 2900*(1-thermal_expansion*(no_intrusive_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % crust density:kg/m^3
        else % mantle
            no_intrusive_rho(i,j) = 3300*(1-thermal_expansion*(no_intrusive_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % mantle density:kg/m^3
        end
        
        
    end
end

% debug
%{
figure(2)
clf;
hold on;
imagesc(x/1e3, y/1e3, no_intrusive_temp)
axis ij image
axis([0 xsize/1e3 0 ysize/1e3])
xlabel('Distance(km)','FontSize', 14)
ylabel('Depth(km)','FontSize', 14)
title('Initial Density Distribution')
box on;
shading interp;
h = colorbar;
%caxis([2500 3500])
h.Label.String = 'Density(kg/m^3)';
%}


%% initial temperature and density distrubtion: when Intrusion begin



% initilize
initial_temp = zeros(ynum, xnum); % initial temperature:K
initial_rho = zeros(ynum, xnum); % initial density
Ts = zeros(ynum, xnum); % solidus temperature;
Tl = zeros(ynum, xnum); % liquidus temperature;
Tll = zeros(ynum, xnum); % lherzolite liquidus temperature;

% type of rock: 1--upper crust, 2--lower crust, 3--mantle, 4--intrusive mantle,
%               5--melted crust, 6--melted mantle, 7--melted intrusive mantle

% nodes cycle
for j = 1:1:xnum
    for i = 1:1:ynum
        
        h = (i-1)*dy; % depth [m]
        P = h/28e3*1e9;  %pressure [Pa]
        PG = h/28e3;  %pressure [GPa]
        initial_temp(i, j) = y(i)/ysize*Tm + T0; % temperature [K]
        
        switch MI(i,j)
            
            
            case 1  % upper crust
                
                initial_rho(i,j) = 2800*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % crust density [kg/m^3]
                
                if P < 1200e6
                    Ts(i,j) = 889 + 17900/(P/1e6+54) + 20200/(P/1e6+54)^2;
                else
                    Ts(i,j) = 831 + 0.06*P/1e6;
                end
                Tl(i,j) = 1262+0.09*P/1e6;
                
            case {2,5} % lower crust
                
                initial_rho(i,j) = 2900*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % crust density [kg/m^3]
                
                if P < 1600e6
                    Ts(i,j) = 973-70400/(P/1e6+354)+77800000/(P/1e6+354)^2;
                else
                    Ts(i,j)= 935 + 0.0035*P/1e6+0.0000062*(P/1e6)^2;
                end
                Tl(i,j) = 1423+0.105*P/1e6;
                
            case  {3,6}  % mantle
                
                initial_rho(i,j) = 3300*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % mantle density [kg/m^3]
                
                % Solidus Temperature, from katz, 2003
                F = 0; % no melt
                X_H20 = X_H20_bulk_mantle/(D_H20+F*(1-D_H20));  % dissovled water fraction in the melt
                X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
                delta_T = K*X_H20^(gama); % C
                T_solidus = 1085.7+132.9.*PG-5.1.*PG.^2-delta_T; %C
                T_sat = 1085.7+132.9.*PG-5.1.*PG.^2 - K.*(X_sat).^(gama);%C
                if X_H20 < X_sat
                    Ts(i,j) = T_solidus + T0;
                else
                    Ts(i,j) = T_sat + T0;
                end
                
                % liquidus Temperature, from katz, 2003
                F = 1; % complete melt
                X_H20 = X_H20_bulk_mantle/(D_H20+F*(1-D_H20));  % dissovled water fraction in the melt
                X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
                delta_T = K*X_H20^(gama); % C
                T_liquidus_lherz = 1475.0+80.0.*PG-3.2.*PG.^2-delta_T; %C
                T_liquidus = 1780+45.*PG-2.*PG.^2-delta_T; %C
                T_sat = 1085.7+132.9.*PG-5.1.*PG.^2 - K.*(X_sat).^(gama);%C
                if X_H20 < X_sat
                    Tl(i,j) = T_liquidus + T0; %K
                    Tll(i,j) = T_liquidus_lherz + T0;
                else
                    Tl(i,j) = T_sat + T0; %K
                    Tll(i,j) = T_sat + T0; %K
                end
                
            case  {4,7}   % underplating
                
                initial_temp(i, j) = Tp + T0; % intrusive magma temperature(the same): K
                initial_rho(i,j) = 3250*(1-thermal_expansion*(initial_temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % intrusive magma density:kg/m^3
                
                % Solidus Temperature, from katz, 2003
                F = 0; % no melt
                X_H20 = X_H20_bulk_intrusion/(D_H20+F*(1-D_H20));  % dissovled water fraction in the melt
                X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
                delta_T = K*X_H20^(gama); % C
                T_solidus = 1085.7+132.9.*PG-5.1.*PG.^2-delta_T; %C
                T_sat = 1085.7+132.9.*PG-5.1.*PG.^2 - K.*(X_sat).^(gama);%C
                if X_H20 < X_sat
                    Ts(i,j) = T_solidus + T0; %K
                else
                    Ts(i,j) = T_sat + T0; %K
                end
                
                % liquidus Temperature, from katz, 2003
                F = 1; % complete melt
                X_H20 = X_H20_bulk_intrusion/(D_H20+F*(1-D_H20));  % dissovled water fraction in the melt
                X_sat = (chi1.*PG.^(lambda)+chi2.*PG);
                delta_T = K*X_H20^(gama); % C
                T_liquidus = 1780+45.*PG-2.*PG.^2-delta_T; %C
                T_liquidus_lherz = 1475.0+80.0.*PG-3.2.*PG.^2-delta_T; %C
                T_sat = 1085.7+132.9.*PG-5.1.*PG.^2 - K.*(X_sat).^(gama);%C
                if X_H20 < X_sat
                    Tl(i,j) = T_liquidus + T0; %K
                    Tll(i,j) = T_liquidus_lherz + T0;
                else
                    Tl(i,j) = T_sat + T0; %K
                    Tll(i,j) = T_sat + T0; %K
                end
                
            otherwise
                
                print('Material type Wrong');
                
        end
        
        
    end
end


% debug
%{
figure(2);
clf;
hold on;
imagesc(x/1e3, y/1e3, initial_rho)
axis ij image
axis([0 xsize/1e3 0 ysize/1e3])
xlabel('Distance(km)','FontSize', 14)
ylabel('Depth(km)','FontSize', 14)
title('Initial Density Distribution')
box on;
shading interp;
h = colorbar;
caxis([2500 3500])
h.Label.String = 'Density(kg/m^3)';
%}

%{
figure(2);
clf;
hold on;
imagesc(x/1e3, y/1e3, initial_temp)
axis ij image
axis([0 xsize/1e3 0 ysize/1e3])
xlabel('Distance(km)','FontSize', 14)
ylabel('Depth(km)','FontSize', 14)
title('Initial Density Distribution')
box on;
shading interp;
h = colorbar;
h.Label.String = 'Temperature(K)';
%}

% debug
%{
figure(1)
clf;
hold on;
plot(Ts(:,round(xnum/2))-T0, y/1e3,'g--')
plot(Tl(:,round(xnum/2))-T0, y/1e3,'b--')
plot(Tll(:,round(xnum/2))-T0, y/1e3,'y--')
plot(initial_temp(:,round(xnum/2))-T0, y/1e3,'r')
legend('solidus','liquidus')
hold off;
axis ij
xlabel('Temperature(C)')
ylabel('Depth(km)')
set(gca,'xaxislocation','top')
axis([0 2000 0 ysize/1e3])
box on;
title('Solidus, liquidus(^\circ C)')
%}


%% Initial surface heat flux
initial_heat_flux = zeros(1, xnum); % heat flux: mW/m^2
for j = 1:1:xnum 
    thermal_conductivity_surface = (0.64+807/(initial_temp(2,j)+77))*exp(0.00004*0/1e6); % thermal conductivity [W/m/K]
    initial_heat_flux(j) = thermal_conductivity_surface*(initial_temp(3,j)-initial_temp(1,j))/dy/2*1000; % heat flux [mW/m^2]
end

% debug
%{
figure(1)
clf;
plot(x/1e3, initial_heat_flux,'g--')
xlabel('Xsize(KM)')
ylabel('Heat flux(mW/m^2)')
box on;
axis([0 xsize/1e3 0 50])
title('Initial heat flux')
%}



%% determining intial field strength i.e. 0 m.y.(Note : no intrusive magma!!!! )
%
sigma_b = zeros(ynum, xnum); % brittle stength [Pa], in the form of stress difference!!!!
sigma_d = zeros(ynum, xnum); % ductile strength [Pa]
for j = 1:1:xnum
    for i = 1:1:ynum
        
        % calculating brittle strength(MPa)

        h = (i-1)*dy; % depth: m
        P = h/28e3*1e3;  %pressure: MPa
        if P <= 120
            sigma_b(i,j) = 3.9/4.9*P;  % MPa
        else
            sigma_b(i,j) = 2.1/3.1*P+100*2.1/3.1;  % Mpa
        end
        
        % calculating ductile strength(MPa)
        if  y(i) < uppercrust          % upper crust
            sigma_d(i,j) = (sr/10^(-3.5))^(1/2.3)*exp(154e3/Rgas/no_intrusive_temp(i,j)/2.3); % ductile strength(MPa)
        elseif  y(i) <= moho_depth && y(i) > uppercrust       % lower crust
            sigma_d(i,j) = (sr/3.3e-4)^(1/3.2)*exp(238e3/Rgas/no_intrusive_temp(i,j)/3.2); % ductile strength(MPa)
        else               % mantle
            sigma_d(i,j) = (sr/10^(4.4))^(1/3.5)*exp(532e3/Rgas/no_intrusive_temp(i,j)/3.5);
        end
        
    end
end

sigma = min(sigma_b, sigma_d);
initial_sigma = sigma(:,round(xnum/2));
initial_sigma_int = sum(sigma)*dy; % integration(N/M)


% debug
%{
figure(301);
clf;
axis ij;
hold on;
plot(sigma_d(:,1), y/1e3,'y--');
plot(sigma_b(:,1), y/1e3,'g--');
plot(sigma(:,1), y/1e3,'r');
hold off;
axis([0 1500 0 ysize/1e3])
xlabel('Yield Strength(MPa)')
ylabel('Depth(km)')
box on;
%}

%debug
%{
figure(3);
clf; % clearing the figure
hold on;
plot(x/1e3, initial_sigma_int, 'r')
title('Initial Yield Strength Integration')
xlabel('Distance(km)')
ylabel('Yield Strength Integration(N/M)')
box on;
%}


%% Using Finte Difference to determine temperature

t = 0; % total time of simulation: s
temp = initial_temp; % temperature of upwelling mantle: K
rho = initial_rho; % density distribution: kg/m^3
melt_percentage = zeros(ynum, xnum); % melt percentage

% save data
save_sigma = zeros(ynum,tnum);
save_underplating_thickness = zeros(tnum, 1);
save_underplating_melt_percentage = zeros(tnum, 1);
save_granitic_thickness = zeros(tnum, 1);
save_subsidence = zeros(xnum, tnum);
save_sigma_int = zeros(xnum, tnum);
save_heat_flux = zeros(xnum, tnum);
save_subsidence_rate = zeros(xnum, tnum);

% initialize sparse matrix: non-zeros store in values, its row and column index store in rows and columns
%sparse_size = (xnum*ynum-((xnum-2+ynum-2)*2+4))*5+xnum*2+2*2*(ynum-2);
%values = zeros(sparse_size,1);
%rows = zeros(sparse_size,1);
%columns = zeros(sparse_size,1);
%si = 1; % dummy number

for k = 1:1:tnum
    
    t = t + dt; % time: second
    tmy = t/(1e6*365.25*24*3600); % time: million year
    L = sparse(ynum*xnum, ynum*xnum);  % left matrix
    R = zeros(ynum*xnum, 1);   % right matrix
    for j = 1:1:xnum
        for i = 1:1:ynum
            
            kk = ynum*(j-1) + i; % index for nodes at (i,j)
            h = (i-1)*dy; % depth: m
            P = h/28e3*1e9;  % pressure: Pa
            
            % applying boundary condition
            if y(i) == 0 || x(j) == 0 || x(j) == xsize || y(i) == ysize  % four boundaries
                
                if y(i) == 0 || y(i) == ysize % left and right boundary: constant temperature condition
                    L(kk,kk) = 1;
                    R(kk) = initial_temp(i,j); % K
                elseif x(j) == 0   % right boundary
                    L(kk,kk) = 1;
                    L(kk,kk+ynum) = -1;
                    R(kk) = 0;
                elseif x(j) == xsize   % left boundary
                    L(kk,kk) = 1;
                    L(kk,kk-ynum) = -1;
                    R(kk) = 0;
                end
                
                % internal nodes
            else
                
                % determing Aro, ft, thermal conductivity and melt
                % percentage
                switch MI(i,j)
                    
                    case 1  % upper crust
                        
                        Aro = 1e-6*exp(-3.0*1e-4*tmy); % crust temperature: volumetric radioactive heating: W/m^3(taras, 2007)
                        thermal_conductivity = (0.64+807/(temp(i,j)+77))*exp(0.00004*P/1e6); % thermal conductivity: W/m/K
                        ft = 1/(Tl(i,j)-Ts(i,j));
                        
                    case {2,5} % lower crust
                        
                        Aro = 0.25e-6*exp(-3.0*1e-4*tmy); % crust temperature: volumetric radioactive heating: W/m^3(taras, 2007)
                        thermal_conductivity = (1.18+474/(temp(i,j)+77))*exp(0.00004*P/1e6); % thermal conductivity: W/m/K
                        ft = 1/(Tl(i,j)-Ts(i,j));
                        % set melted crust
                        if temp(i,j) > Ts(i,j)
                            melt_percentage(i,j) = (temp(i,j)-Ts(i,j))/(Tl(i,j)-Ts(i,j));
                            MI(i,j) = 5;
                        end
                        
                    case  {3,6}  % mantle
                        
                        Aro = 0.022e-6*exp(-3.0*1e-4*tmy); % mantle temperature: volumetric radioactive heating: W/m^3(taras, 2007)
                        thermal_conductivity = (0.73+1293/(temp(i,j)+77))*exp(0.00004*P/1e6); % thermal conductivity: W/m/K
                        [ft, F] = melt_percentage_katz(P,temp(i,j)-T0,X_H20_bulk_mantle,modal,TOL); % F is melt percentage(0-1), ft is parameter
                        melt_percentage(i,j) = F;
                        % set melted mantle
                        if F > TOL
                            MI(i,j) = 6;
                        end
                        
                    case  {4,7}   % underplating
                        
                        Aro = 0.022e-6*exp(-3.0*1e-4*tmy); % intrusive magma: volumetric radioactive heating: W/m^3(taras, 2007)
                        thermal_conductivity = (0.73+1293/(temp(i,j)+77))*exp(0.00004*P/1e6); % thermal conductivity: W/m/K
                        [ft, F] = melt_percentage_katz(P,temp(i,j)-T0,X_H20_bulk_intrusion,modal,TOL); % F is melt percentage(0-1), ft is parameter
                        melt_percentage(i,j) = F;
                        % set melted underplating material
                        if F > TOL
                            MI(i,j) = 7;
                        end
                        
                    otherwise
                        
                        print('Material type Wrong');
                      
                end
                
                fx = 1 + La/Cp*real(ft);
                kappa = thermal_conductivity/rho(i,j)/Cp; % thermal diffusivity
                mux = kappa*dt/dy^2; % coefficient
                muy = kappa*dt/dx^2;  % coefficient
                L(kk, kk) = fx + mux + muy;
                L(kk, kk+1) = -1/2*mux;
                L(kk, kk-1) = -1/2*mux;
                L(kk, kk+ynum) = -1/2*muy;
                L(kk, kk-ynum) = -1/2*muy;
                R(kk) = 1/2*mux*(temp(i,j+1)+temp(i,j-1))+1/2*muy*(temp(i-1,j)+temp(i+1,j))+(fx-mux-muy)*temp(i,j) + Aro*exp(-y(i)/d)/rho(i,j)/Cp*dt;
                
            end
        end
    end
    
    % debug
    %{
    figure(5);
    clf;
    pcolor(x/1e3, y/1e3, melt_percentage)
    axis ij;
    xlabel('melt percentage(%)')
    ylabel('Depth(km)')
    set(gca,'xaxislocation','top');
    box on;
    shading interp;
    h = colorbar;
    %}
    
    %% determing temperature and plot
    temp = reshape(L\R, [ynum, xnum]);
    
    %{
    %debug
    pcolor(x/1e3, y/1e3, temp)
    axis ij
    axis equal
    axis([0 xsize/1e3 0 ysize/1e3])
    xlabel('Distance(km)','FontSize', 14)
    ylabel('Depth(km)','FontSize', 14)
    box on;
    shading interp;
    h = colorbar;
    h.Label.String = 'Temperature(\circC)';
    %}
    
    %% showing the result
    %
    %set(0,'DefaultFigureVisible', 'off')
    %
    if mod(k,1) == 0

        % Density 2D
        figure(6);
        %figure('visible','off')
        clf; % clearing the figure
        pcolor(x/1e3, y/1e3, rho)
        axis([0 xsize/1e3 0 ysize/1e3])
        axis ij;
        axis image;
        xlabel('Distance(km)')
        ylabel('Depth(km)')
        set(gca,'xaxislocation','top')
        shading interp;
        colorbar;
        %caxis([2500 3500])
        title(['Density (kg/m^3) at step = ',num2str(k),'time, Myr=',num2str(tmy)])
    
        
        % Temp 1D
        figure(7);
        %figure('visible','off')
        clf;
        hold on;
        plot(temp(:,round(xnum/2))-T0, y/1e3, 'r')
        plot(Ts(:,round(xnum/2))-T0, y/1e3, 'b')
        plot(Tl(:,round(xnum/2))-T0, y/1e3, 'y')
        plot(initial_temp(:, round(xnum/2))-T0, y/1e3, 'g')
        legend('temp','solidus','liquidus','initial')
        xlabel('Temperature(^\circC)');
        ylabel('Depth(km)');
        set(gca,'xaxislocation','top');
        axis([0 2000 0 ysize/1e3])
        axis ij;
        box on;
        hold off;
        title(['Temp(^\circC) at step = ',num2str(k),'time, Myr=',num2str(tmy)])
        
        
        % Temp 2D
        figure(8);
        %figure('visible','off')
        clf; % clearing the figure
        pcolor(x/1e3, y/1e3, temp)
        axis([0 xsize/1e3 0 ysize/1e3])
        axis ij image;
        xlabel('Distance(km)')
        ylabel('Depth(km)')
        set(gca,'xaxislocation','top')
        shading interp;
        colorbar;
        title(['Temp(^\circC) 2D at step = ',num2str(k),'time, Myr=',num2str(tmy)])
        

        % Strength 1D
        figure(9);
        %figure('visible','off')
        clf;
        axis ij;
        hold on;
        plot(sigma_d(:,round(xnum/2)), y/1e3,'y--');
        plot(sigma_b(:,round(xnum/2)), y/1e3,'g--');
        plot(sigma(:,round(xnum/2)), y/1e3,'r');
        hold off;
        box on;
        axis([0 2000 0 ysize/1e3])
        xlabel('Yield Strength(MPa)')
        ylabel('Depth(km)')
        title(['Strength(N/M) at step = ',num2str(k),'time, Myr=',num2str(tmy)])

        % Rock Type 2D
        figure(10);
        %figure('visible','off')
        clf; % clearing the figure
        pcolor(x/1e3, y/1e3, melt_percentage)
        axis([0 xsize/1e3 0 ysize/1e3])
        axis ij image;
        xlabel('Distance(km)')
        ylabel('Depth(km)')
        set(gca,'xaxislocation','top')
        shading interp;
        colorbar;
        %caxis([0 0.2])
        title(['Melt Percentage at step = ',num2str(k),'time, Myr=',num2str(tmy)])
    end
    %}
    
    
    % Stop for 0.1 second
    drawnow % draw above figure without delay
    pause(0.1);
    %}
    sprintf('%d',tmy)
    
    
    
    %% melt percentage and thickness
    underplating_melt_percentage = 0;
    underplating_volume_index = 0;
    granitic_melt_percentage = 0;
    granitic_volume_index = 0;
    granitic_thickness = 0;
    for i = 1:1:ynum
        for j = 1:1:xnum
            
            % underplating
            if MI(i,j)==4 || MI(i,j)==7
                underplating_volume_index = underplating_volume_index + 1;
                underplating_melt_percentage = underplating_melt_percentage + melt_percentage(i,j);            
            elseif MI(i,j) == 5 % lower crust melt
                granitic_volume_index = granitic_volume_index + 1;
                granitic_melt_percentage = granitic_melt_percentage + melt_percentage(i,j);
            end
        end
    end
    
    underplating_thickness = underplating_melt_percentage/(underplating_volume_index)*intrusion_thickness; % the thinkness of underplating
    underplating_melt_percentage = underplating_melt_percentage/underplating_volume_index; % the percentage of melt for underplating
    
    if  granitic_melt_percentage*dx*dy/granitic_volume_index/(top/2+bottom/2) > granitic_thickness
        granitic_thickness = granitic_melt_percentage*dx*dy/granitic_volume_index/(top/2+bottom/2);% the thinkness of granitic
    end
    
    save_underplating_thickness(k) = underplating_thickness;
    save_underplating_melt_percentage(k) = underplating_melt_percentage;
    save_granitic_thickness(k) = granitic_thickness;
    
    %% subsidence
    %
    for i = 1:1:ynum
        for j = 1:1:xnum
            h = (i-1)*dy; % depth: m
            P = h/28e3*1e9;  %pressure: Pa
            
            switch  MI(i,j)
                case 1  % upper crust
                    rho(i,j) = 2800*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % upper crust density:kg/m^3
                case 2 % lower crust
                    rho(i,j) = 2900*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % lower crust density:kg/m^3
                case 5 % lower crust melt
                    rho(i,j) = 2800*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % melt density:kg/m^3
                case 3 % mantle
                    rho(i,j) = 3300*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % mantle density:kg/m^3
                case 6 % mantle melt
                    rho(i,j) = 2700*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % melt density:kg/m^3
                case 4 %intrusion
                    rho(i,j) = 3250*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % intrusive magma density:kg/m^3
                case 7 %intrusion melt
                    rho(i,j) = 3100*(1-thermal_expansion*(temp(i,j)-298))*(1+thermal_compressibility*(P-1e5)); % melt density:kg/m^3
                otherwise
                    print('Material type Wrong');
            end
            
        end
    end
    subsidence = 1/(rhom)*(sum(no_intrusive_rho*dy)-sum(rho*dy)); % integration(m)
    save_subsidence(:, k) = subsidence; % save data;
    if k > 3
        save_subsidence_rate(:, k) = (save_subsidence(:,k)- save_subsidence(:, k-3))/dt/3; % save data, subsidence rate
    end

   
    %
    %begug
    figure(300);
    clf; % clearing the figure
    hold on;
    plot(x/1e3, subsidence(:, k), 'r')
    title(['Uplift at step = ',num2str(k),'time, Myr=',num2str(tmy)])
    xlabel('Uplift(km)')
    ylabel('subsidence(km)')
    axis([0 xsize/1e3 0 1e-11])
    box on;
    hold off;
    box on;
    grid on;
    %}
    
    %% surface heat flux
    heat_flux = zeros(1,xnum); % heat flux: mW/m^2
    for j = 1:1:xnum
        thermal_conductivity_surface = (0.64+807/(temp(2,j)+77))*exp(0.00004*0/1e6); % thermal conductivity: W/m/K
        heat_flux(j) = thermal_conductivity_surface*(temp(3,j)-temp(1,j))/dy/2*1000; % heat flux: mW/m^2
    end
    % save data
    save_heat_flux(:, k) = heat_flux;
    
    % debug
    %{
    figure(1)
    clf;
    plot(x/1e3, heat_flux,'g--')
    xlabel('Xsize(KM)')
    ylabel('Heat flux(mW/m^2)')
    box on;
    axis([0 xsize/1e3 0 50])
    title('Initial heat flux')
    %}

    
    
    %% lithospheric strength
    for j = 1:1:xnum
        for i = 1:1:ynum
            
            % calculating brittle strength(MPa)
            h = (i-1)*dy; % depth: m
            P = h/28e3*1e3;  %pressure: MPa
            if P <= 120
                sigma_b(i,j) = 3.9/4.9*P;  % MPa
            else
                sigma_b(i,j) = 2.1/3.1*P+100*2.1/3.1;  % Mpa
            end
            
            % calculating ductile strength(MPa)
            switch  MI(i,j)
                
                case MI(i,j)==1  % upper crust
                    sigma_d(i,j) = (sr/10^(-3.5))^(1/2.3)*exp(154e3/Rgas/temp(i,j)/2.3); % ductile strength(MPa)
                case {2, 5} % lower crust
                    sigma_d(i,j) = (sr/3.3e-4)^(1/3.2)*exp(238e3/Rgas/temp(i,j)/3.2); % ductile strength(MPa)
                case {3,6} % mantle
                    sigma_d(i,j) = (sr/10^(4.4))^(1/3.5)*exp(532e3/Rgas/temp(i,j)/3.5);
                case {4,7}
                    sigma_d(i,j) = (sr/10^(3.3))^(1/4)*exp(470e3/Rgas/temp(i,j)/4);
                otherwise
                    print('Material type Wrong');
            end
            
        end
    end
    
    % field strength: choose the smaller one
    sigma = min(sigma_b, sigma_d);
    sigma_int = sum(sigma)*dy; % integration(N/M)
    save_sigma_int(:, k) = sigma_int;
    save_sigma(:,k) = sigma(:,round(xnum/2));
    %{
    figure(3);
    clf; % clearing the figure
    hold on;
    plot(x/1e3, initial_sigma_int*1e6, 'b')
    plot(x/1e3, sigma_int*1e6, 'r')
    title('Yield Strength Integration')
    xlabel('Distance(km)')
    ylabel('Yield Strength Integration(N/M)')
    box on;
    hold off;
    %}
    
    %sprintf('%d',tmy)
end

%% save data to mat file
str = sprintf('MatData_MODEL%1d',model_number);
str = [str, datestr(now,'yyyy_mm_dd_HH:MM:SS'),'.mat'];
save(str)


%% Temperature profiles at 0, 1, 5, 10, 20m.y. across the lithosphere at center
%plot_data

%% parameter set
wp = 0:300:3000;   % water percentage [ppm]
ut = 1200:100:1700;  % underplating temperature [K]
ui = 20e3:2e3:50e3;  % the thickness of intrusion [m]
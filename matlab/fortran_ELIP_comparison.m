% Author: wang zhenhua 

% This script produces a comparison data set, called Namelist_ELIP, which can be used in fortran
% programs in order to compare the thickness of underplating with different
% water percentage, and the thickness of intrusion.


%% clearing all data
clear;
% clearing command window
clc;

%% model comparison set parameters
% parameter set
wp = 0:50:1500;   % water percentage [ppt]
ut = 1550;  % underplating temperature [K]
ui = 20e3:1e3:50e3;  % the thickness of intrusion [m]


%% open file
fid = fopen('Namelist_ELIP.nml','w');

%% water v.s. intrusion thickness

% temp and intrusion thickness cycle
count = 0; % dummy number
standard_model_setup
Tp = ut; % overwrite Tp as ut
i_dummy = 1; % distinguish i
for water = wp
    
    j_dummy = 1; % distinguish j
    for intrusion_thickness = ui
        
        count = count +1;
        % set parameter in Model7_comparison
        X_H20_bulk_intrusion = water/1000;  % water fraction for intrusion
        X_H20_bulk_mantle = water/1000;     % water fraction for mantle
        
        
        % data
        Namelist_temp = ['&model_control',char(13),'model_number=',num2str(count),',',char(13),'shape=', num2str(shape),',',char(13),'intrusion_thickness=', num2str(intrusion_thickness),',',char(13),'Tp=', num2str(Tp),',',char(13),'X_H20_bulk_intrusion=', num2str(X_H20_bulk_intrusion),',',char(13),'X_H20_bulk_mantle=', num2str(X_H20_bulk_mantle),',',char(13),'/'];
        fprintf(fid,'%s\n\n',Namelist_temp); 
        
        j_dummy = j_dummy +1;                                                                                                                      
    end
    i_dummy = i_dummy + 1;
end 

% close file
fclose(fid);
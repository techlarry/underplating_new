% clear
clear;
clc;
T0 = 273.15; % paramter: K

Model = ['MODEL','1','_'];

% read xy data
x = importdata([Model, 'x_coordinate.dat']);
y = importdata([Model, 'y_coordinate.dat']);

%size
xnum = size(x,2);
ynum = size(y,2);
xsize = x(xnum);
ysize = y(ynum);



%% read temp data
save_temp = importdata([Model, 'save_temp.dat']);
%%
k = 1;
temp = zeros(ynum, xnum);
for i=1:1:ynum
    for j=1:1:xnum
        temp(i,j) = save_temp(k,j+(i-1)*xnum);
    end
end

figure;

subplot(2,3,1)
xlabel('Temperature(^\circ C)')
ylabel('Depth(Km)')
plot(temp(:,10)-T0,y/1e3)
axis ij;
axis([0 1500 0 110])
set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,2)
xlabel('Temperature(^\circ C)')
ylabel('Depth(Km)')
plot(temp(:,round(xnum/2.5))-T0,y/1e3)
axis ij;
axis([0 1500 0 110])
set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,3)
xlabel('Temperature(^\circ C)')
ylabel('Depth(Km)')
plot(temp(:,round(xnum/2))-T0,y/1e3)
axis ij;
axis([0 1500 0 110])
set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,4:6)
imagesc(x/1e3,y/1e3,temp-T0)
shading interp;
h = colorbar;
h.Label.String='Temperature(\circ C)';
%caxis([0 1500])
axis ij image
%title('Initial Temp distribution') 


%% read density data
save_density = importdata([Model,'save_density.dat']);
%%
k = 63;
density = zeros(ynum, xnum);
for i=1:1:ynum
    for j=1:1:xnum
        density(i,j) = save_density(k,j+(i-1)*xnum);
    end
end

subplot(2,3,1)
xlabel('Desnity(kg/m^3)')
ylabel('Depth(Km)')
plot(density(:,10),y/1e3)
axis ij;
axis([2700 3400 0 110])
%set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');



subplot(2,3,2)
xlabel('Desnity(kg/m^3)')
ylabel('Depth(Km)')
plot(density(:,round(xnum/2.5)),y/1e3)
axis ij;
axis([2700 3400 0 110])
%set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,3)
xlabel('Desnity(kg/m^3)')
ylabel('Depth(Km)')
plot(density(:,round(xnum/2)),y/1e3)
axis ij;
axis([2700 3400 0 110])
%set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');




subplot(2,3,4:6)
imagesc(x/1e3,y/1e3,density)
shading interp;
h = colorbar;
h.Label.String='Desnity(kg/m^3)';
caxis([2700 3300])
axis ij image
%title('Density distribution') 



%% initial temperature distribution
initial_temp = importdata([Model,'initial_temp.dat']);

%%
figure(1)
clf;
temp = initial_temp;

subplot(2,3,1)
xlabel('Temperature(^\circ C)')
ylabel('Depth(Km)')
plot(temp(:,10)-T0,y/1e3)
axis ij;
axis([0 1500 0 110])
set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,2)
xlabel('Temperature(^\circ C)')
ylabel('Depth(Km)')
plot(temp(:,round(xnum/2.5))-T0,y/1e3)
axis ij;
axis([0 1500 0 110])
set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,3)
xlabel('Temperature(^\circ C)')
ylabel('Depth(Km)')
plot(temp(:,round(xnum/2))-T0,y/1e3)
axis ij;
axis([0 1500 0 110])
set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,4:6)
imagesc(x/1e3,y/1e3,temp-T0)
shading interp;
h = colorbar;
h.Label.String='Temperature(\circ C)';
%caxis([0 1500])
axis ij image
%title('Initial Temp distribution') 


%% solidus and liquidus
Tl = importdata([Model,'liquidus.dat']);
Ts = importdata([Model,'solidus.dat']);
Tll = importdata([Model,'lherz_liquidus.dat']);
figure(2)
clf;
hold on;
axis ij;
plot(Ts(:,round(xnum/2))-T0, y/1e3,'g--')
plot(Tl(:,round(xnum/2))-T0, y/1e3,'b--')
plot(Tll(:,round(xnum/2))-T0, y/1e3,'y--')
plot(initial_temp(:,round(xnum/2))-T0, y/1e3,'r')
legend('solidus','liquidus','lherzolite liquidus')
hold off;
xlabel('Temperature(C)')
ylabel('Depth(km)')
set(gca,'xaxislocation','top')
axis([0 2000 0 ysize/1e3])
box on;
title('Solidus, liquidus(^\circ C)')

%% Rock Type 2D
% type of rock: 1--upper crust, 2--lower crust, 3--mantle,
% 4--intrusive mantle, 5--melted crust, 6--melted mantle, 7--melted intrusive mantle
MI = importdata([Model,'material_type.dat']);

figure(3)
clf;
pcolor(x/1e3, y/1e3, MI)
axis([0 xsize/1e3 0 ysize/1e3])
axis ij image;
xlabel('Distance(km)')
ylabel('Depth(km)')
set(gca,'xaxislocation','top')
shading interp;
colorbar;
title('Material type')


%% current melt percentage 
melt_percentage = importdata([Model,'melt_percentage.dat']);
%%
figure(4)
clf;
pcolor(x/1e3,y/1e3,melt_percentage)
shading interp;
colorbar;
axis ij image;
h = colorbar;
h.Label.String='Melt Percentage';
caxis([0 0.55])
title('Melt percentage(Ma)')
title('current temp distribution(^\circC)')
%

%% initial density distribution
density = importdata([Model,'initial_density.dat']);
figure(5)
clf;
imagesc(x/1e3,y/1e3,density)
shading interp;
colorbar;
axis ij image;
%caxis([0 1500])
title('initial density distribution(kg/m^3)')
xlabel('Distance(Km)')
ylabel('Depth(km)')
%


subplot(2,3,1)
xlabel('Desnity(kg/m^3)')
ylabel('Depth(Km)')
plot(density(:,10),y/1e3)
axis ij;
axis([2700 3400 0 110])
%set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');



subplot(2,3,2)
xlabel('Desnity(kg/m^3)')
ylabel('Depth(Km)')
plot(density(:,round(xnum/2.5)),y/1e3)
axis ij;
axis([2700 3400 0 110])
%set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');


subplot(2,3,3)
xlabel('Desnity(kg/m^3)')
ylabel('Depth(Km)')
plot(density(:,round(xnum/2)),y/1e3)
axis ij;
axis([2700 3400 0 110])
%set(gca,'xtick',[0 500 1000 1500]);
set(gca,'XAxisLocation','top');




subplot(2,3,4:6)
imagesc(x/1e3,y/1e3,density)
shading interp;
h = colorbar;
h.Label.String='Desnity(kg/m^3)';
caxis([2700 3300])
axis ij image
%title('Density distribution') 




%%  no_intrusive_rho v.s. initial_rho
no_intrusive_rho = importdata([Model,'no_intrusive_rho.dat']);
no_intrusive_rho_sum = sum(no_intrusive_rho,1);
initial_rho = importdata([Model,'initial_density.dat']);
initial_rho_sum = sum(initial_rho,1);
plot(1:xnum,no_intrusive_rho_sum)
hold on
plot(1:xnum,initial_rho_sum)
hold off
figure
imagesc(initial_rho-no_intrusive_rho)


%%  no_intrusive_temp v.s. initial_temp
no_intrusive_temp = importdata([Model,'no_intrusive_temp.dat']);
no_intrusive_temp_sum = sum(no_intrusive_temp,1);
initial_temp = importdata([Model,'initial_temp.dat']);
initial_temp_sum = sum(initial_temp,1);
figure;
imagesc(no_intrusive_temp)
figure;
plot(1:xnum,no_intrusive_temp_sum)
hold on
plot(1:xnum,initial_temp_sum)
hold off



%% melt percentage distribution
save_melt = importdata([Model,'save_melt.dat']);
%%
figure(7)
clf;
k = 1;
melt = zeros(ynum, xnum);
for i=1:1:ynum
    for j=1:1:xnum
        melt(i,j) = save_melt(k,j+(i-1)*xnum);
    end
end
imagesc(x/1e3,y/1e3,melt)
colorbar;
axis ij image;
h = colorbar;
h.Label.String='Melt Percentage';
caxis([0 0.55])
title('Melt percentage(1.2Ma)')


%%   subsidence
subsidence = importdata([Model,'subsidence.dat']);
subsidence = subsidence';
%%
figure(122)
clf;

%axis([0 600 0 1.5])

% sendiment and water density
rhos = 2300; % sendiment denisty: kg/m^3
rhow = 1030; % water denisty: kg/m^3
rhol = 2800; % lithoshere denisty: kg/m^3
rhom = 3300; % mantle denisty: kg/m^3

hold on

k = 127;
sk = subsidence(:,k)/1e3*rhom/rhow;
yy = smooth(x,sk,25,'moving');
plot(x/1e3, yy)
box on;
xlabel('x(km)');
ylabel('Uplift(km)');

k = 1266;
sk = subsidence(:,k)/1e3*rhom/rhow;
yy = smooth(x,sk,25,'moving');
plot(x/1e3, yy)
box on;
xlabel('x(km)');
ylabel('Uplift(km)');

k = 6330;
sk = subsidence(:,k)/1e3*rhom/rhow;
yy = smooth(x,sk,25,'moving');
plot(x/1e3, yy)
box on;
xlabel('x(km)');
ylabel('Uplift(km)');

k = 20000;
sk = subsidence(:,k)/1e3*rhom/rhow;
yy = smooth(x,sk,25,'moving');
plot(x/1e3, yy)
box on;
xlabel('x(km)');
ylabel('Uplift(km)');
hold off

legend('1Ma','10Ma','50Ma','160Ma')


%%
figure(110)
colormap hsv
t = 0.0079:0.0079:79;
x = x/1e3;
[T, X] = meshgrid(t,x);
z = subsidence(:,1:10000);
zz = smooth(repmat(x,[601,10000]),z,25,'moving');

%%
surf(x,t,zz','FaceColor','interp','EdgeColor','none','FaceLighting','gouraud')
xlabel('X coordinate, Km')
ylabel('Time, m.y.')
zlabel('Topography, m')
camlight left


%%   heat flux
heat_flux = importdata([Model,'heat_flux.dat']);
heat_flux = heat_flux';
%%
figure(123)
hold on 

k = 127;
plot(x(1:xnum-9)/1e3, heat_flux(:,k))
box on;

k = 1266;
plot(x(1:xnum-9)/1e3, heat_flux(:,k))
box on;

k = 12658;
plot(x(1:xnum-9)/1e3, heat_flux(:,k))
box on;

k = 50000;
plot(x(1:xnum-9)/1e3, heat_flux(:,k))
box on;

xlabel('x(km)');
ylabel('heat flux(mW/m^2)');

legend('1Ma','10Ma','100Ma','300Ma')
title('Heat Flux')

%%   save_sigma
save_sigma = importdata([Model,'save_sigma.dat']);
initial_sigma = importdata([Model,'initial_sigma.dat']);
ma0=[initial_sigma;linspace(1,size(y,2),size(y,2))]';
%%
figure(124)
clf;
hold on;
plot(initial_sigma, y(1:ynum)/1e3)
axis ij
%%figure and save data

k = 127;
plot( save_sigma(k,:), y(1:ynum)/1e3)
ma1=[save_sigma(k,:);linspace(1,size(y,2),size(y,2))]';

k = 1266;
plot( save_sigma(k,:), y(1:ynum)/1e3)
ma10=[save_sigma(k,:);linspace(1,size(y,2),size(y,2))]';

k = 12658;
plot( save_sigma(k,:), y(1:ynum)/1e3)
ma100=[save_sigma(k,:);linspace(1,size(y,2),size(y,2))]';

k = 20000;
plot( save_sigma(k,:), y(1:ynum)/1e3)
ma160=[save_sigma(k,:);linspace(1,size(y,2),size(y,2))]';
hold off;


axis ij;
box on;
xlabel('\sigma_1-\sigma_3(Mpa)');
ylabel('Depth(km)');
legend('Initial','1Ma','10Ma','100Ma','160Ma')

title('byerlee law')





%%   save_sigma_int
save_sigma_int = importdata([Model,'save_sigma_int.dat']);
%%
figure(123)
clf;

k = 127;
sk = save_sigma_int(k,:);
yy = smooth(x,sk/10^(7),10,'moving');
plot(x/1e3,yy)
hold on;

k = 1266;
sk = save_sigma_int(k,:);
yy = smooth(x,sk/10^(7),10,'moving');
plot(x/1e3,yy)
hold on;

k = 12658;
sk = save_sigma_int(k,:);
yy = smooth(x,sk/10^(7),10,'moving');
plot(x/1e3,yy)
hold on;

k = 20000;
sk = save_sigma_int(k,:);
yy = smooth(x,sk/10^(7),10,'moving');
plot(x/1e3,yy)
hold off;

box on;
xlabel('x(km)');
ylabel('Lithospheric Strength(10^{12}N/M)');
legend('1Ma','10Ma','100Ma','160Ma')

%%   save_underplating_thickness
save_underplating_thickness = importdata([Model,'save_underplating_thickness.dat']);
figure(123)
clf;
plot(save_underplating_thickness/1e3)
box on;
xlabel('time step');
ylabel('underplating_thickness(Km)');
save_underplating_thickness(1)
